package cn.jhz.learn.community_dynamic.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.jhz.learn.community_dynamic.common.util.MineCommonUtil;
import cn.jhz.learn.community_dynamic.dao.PostJpaRepository;
import cn.jhz.learn.community_dynamic.dao.TopicClassJpaRepository;
import cn.jhz.learn.community_dynamic.dao.TopicJpaRepository;
import cn.jhz.learn.community_dynamic.dao.UserJpaRepository;
import cn.jhz.learn.community_dynamic.model.TopicClassEntity;

import lombok.Getter;
import lombok.ToString;

@Service
public class StatsService {

    private final UserJpaRepository userJpaRepository;
    private final TopicClassJpaRepository topicClassJpaRepository;
    private final TopicJpaRepository topicJpaRepository;
    private final PostJpaRepository postJpaRepository;

    @Getter
    @ToString
    private class ChartView {
	private String name;
	private List<Object> data;
    }
    

    @Autowired
    public StatsService(UserJpaRepository userJpaRepository, TopicClassJpaRepository topicClassJpaRepository,
	    TopicJpaRepository topicJpaRepository, PostJpaRepository postJpaRepository) {
	this.userJpaRepository = userJpaRepository;
	this.topicClassJpaRepository = topicClassJpaRepository;
	this.topicJpaRepository = topicJpaRepository;
	this.postJpaRepository = postJpaRepository;
    }

    public Collection<ChartView> countAllLinePostCount() {
	List<TopicClassEntity> topicEntities = this.topicClassJpaRepository.findAll();
	Map<String, ChartView> topicClassMap = new HashMap<>();
	Date openedYear = MineCommonUtil.getYear(this.postJpaRepository.findTopByOrderByCreateTimeAsc().get().getCreateTime());

	topicEntities.forEach(entity -> topicClassMap.put(entity.getClassName(), this.lineAdpt(entity.getClassName(), new ArrayList<>())));
	
	    int length = MineCommonUtil.yearSubtract(MineCommonUtil.getCurrentYear(), openedYear);
	    int i = 0;
	    do {
		for (TopicClassEntity entity : topicEntities) {
		    topicClassMap.get(entity.getClassName()).getData()
			    .add(this.postJpaRepository.countByTopics_topicClassAndStatusAndCreateTimeBetween(entity,
				    (byte) 1, MineCommonUtil.getYear(openedYear, i),
				    MineCommonUtil.getYear(openedYear, i + 1)));
		}
		i++;
	    } while (i <= length);

	return topicClassMap.values();
    }

    public List<Object> countAllPiePostCount() {
	List<TopicClassEntity> topicEntities = this.topicClassJpaRepository.findAll();
	
	return topicEntities.stream().map(entity-> this.pieAdpt(entity.getClassName(), this.postJpaRepository.countByTopics_topicClassAndStatus(entity, (byte) 1))).collect(Collectors.toList());
    }
    
    private ChartView lineAdpt(String name, List<Object> data) {
	ChartView view = new ChartView();
	view.name = name;
	view.data = data;
	return view;
    }
    
    private List<?> pieAdpt(Object name, Object data) {
	List<Object> view = new ArrayList<>();
	view.add(name);
	view.add(data);
	return view;
    }
}
