package cn.jhz.learn.community_dynamic.service;

import cn.jhz.learn.community_dynamic.common.exception.*;
import cn.jhz.learn.community_dynamic.manager.EmailManager;
import cn.jhz.learn.community_dynamic.manager.SMSManager;
import cn.jhz.learn.community_dynamic.manager.VerificationCodeManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CodeService {

    private final static String templateUpperPart = "<head><base target=\"_blank\" /><style type=\"text/css\">::-webkit-scrollbar{ display: none; }</style><style id=\"cloudAttachStyle\" type=\"text/css\">#divNeteaseBigAttach, #divNeteaseBigAttach_bak{display:none;}</style><style id=\"blockquoteStyle\" type=\"text/css\">blockquote{display:none;}</style><style type=\"text/css\">body{font-size:14px;font-family:arial,verdana,sans-serif;line-height:1.666;padding:0;margin:0;overflow:auto;white-space:normal;word-wrap:break-word;min-height:100px}td, input, button, select, body{font-family:Helvetica, 'Microsoft Yahei', verdana}pre {white-space:pre-wrap;white-space:-moz-pre-wrap;white-space:-pre-wrap;white-space:-o-pre-wrap;word-wrap:break-word;width:95%}th,td{font-family:arial,verdana,sans-serif;line-height:1.666}img{ border:0}header,footer,section,aside,article,nav,hgroup,figure,figcaption{display:block}blockquote{margin-right:0px}</style></head><body tabindex=\"0\" role=\"listitem\"><table width=\"700\" border=\"0\" align=\"center\" cellspacing=\"0\" style=\"width:700px;\"><tbody><tr><td><div style=\"width:700px;margin:0 auto;border-bottom:1px solid #ccc;margin-bottom:30px;\"><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"700\" height=\"39\" style=\"font:12px Tahoma, Arial, 宋体;\"><tbody><tr><td width=\"210\"></td></tr></tbody></table></div><div style=\"width:680px;padding:0 10px;margin:0 auto;\"><div style=\"line-height:1.5;font-size:14px;margin-bottom:25px;color:#4d4d4d;\"><strong style=\"display:block;margin-bottom:15px;\">尊敬的用户：<span style=\"color:#f60;font-size: 16px;\"></span>您好！</strong><strong style=\"display:block;margin-bottom:15px;\">您正在进行<span style=\"color: red\">邮箱绑定</span>操作，请在验证码输入框中输入：<span style=\"color:#f60;font-size: 24px\">";
    private final static String templateLowerHalf = "</span>，以完成操作</strong></div><div style=\"margin-bottom:30px;\"><small style=\"display:block;margin-bottom:20px;font-size:12px;\"><p style=\"color:#747474;\">注意：此操作可能会修改您的密码、登录邮箱或绑定手机。如非本人操作，请及时登录并修改密码以保证帐户安全<br>（工作人员不会向你索取此验证码，请勿泄漏！)</p></small></div></div><div style=\"width:700px;margin:0 auto;\"><div style=\"padding:10px 10px 0;border-top:1px solid #ccc;color:#747474;margin-bottom:20px;line-height:1.3em;font-size:12px;\"><p>此为系统邮件，请勿回复<br>请保管好您的邮箱，避免账号被他人盗用</p><p>MACHINE005-社区APP</p></div></div></td></tr></tbody></table></body>";

    private final SMSManager smsManager;
    private final EmailManager emailManager;
    private final VerificationCodeManager verificationCodeManager;

    @Autowired
    public CodeService(SMSManager smsManager, EmailManager emailManager,
	    VerificationCodeManager verificationCodeManager) {
	this.smsManager = smsManager;
	this.emailManager = emailManager;
	this.verificationCodeManager = verificationCodeManager;
    }

    public void sendVerificationCodeByPhone(String phone) {
	/* 判断是否已经获取过验证码 */
	if (this.verificationCodeManager.getInstance(phone).isPresent()) {
	    /* 已获取 */
	    throw new VerificationCodeAcquisitionExceptions("验证码已发送!");
	} else {
	    /* 首次获取 */
	    // 生成随机验证码
	    String code = this.verificationCodeManager.newInstance(phone);
	    // 发送验证码
	    this.smsManager.sendGeneralVerification(phone, code);
	    // 发送成功，写入缓存
	    this.verificationCodeManager.cacheInstance(phone, code);

	}
    }

    public void sendVerificationCodeByEmail(String email) {
	/* 判断是否已经获取过验证码 */
	if (this.verificationCodeManager.getInstance(email).isPresent()) {
	    /* 已获取 */
	    throw new VerificationCodeAcquisitionExceptions("验证码已发送!");
	} else {
	    /* 首次获取 */
	    // 生成随机验证码
	    String code = this.verificationCodeManager.newInstance(email);
	    // 发送验证码
	    this.emailManager.sendMimeMessge(email, "绑定社区验证码", newCodeTemplate(code));
	    // 发送成功，写入缓存
	    this.verificationCodeManager.cacheInstance(email, code);
	}
    }

    static private String newCodeTemplate(String code) {
	StringBuffer buffer = new StringBuffer(CodeService.templateUpperPart);
	buffer.append(code);
	buffer.append(CodeService.templateLowerHalf);
	return buffer.toString(); 
    }
}
