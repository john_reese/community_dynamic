package cn.jhz.learn.community_dynamic.service;

import java.util.List;
import java.util.Optional;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import cn.jhz.learn.community_dynamic.model.CommentEntity;

@Service
public class CommentService {

    public CommentEntity review(CommentEntity entity) {
	// TODO 校验
	return entity;
    }
    
    @Cacheable(cacheNames = "reviewComment", key = "#key", unless = "#result == null")
    public Optional<List<Integer>> cacheReview(String key, List<Integer> cache) {
	return Optional.ofNullable(cache);
    }
    
    @CacheEvict(cacheNames="reviewComment", allEntries=true)
    public void clearReviewCache(String key){}
    
    @CachePut(cacheNames = "reviewComment", key = "#key", unless = "#result == null")
    public Optional<List<Integer>> putReviewCache(String key, List<Integer> cache){
        return Optional.ofNullable(cache);
    }

    @Cacheable(cacheNames = "reviewComment", unless = "#result == null")
    public Optional<List<Integer>> getReviewCache(String key){
        return Optional.empty();
    }
}
