package cn.jhz.learn.community_dynamic.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import cn.jhz.learn.community_dynamic.dao.ImageJpaRepository;
import cn.jhz.learn.community_dynamic.manager.FileManager;
import cn.jhz.learn.community_dynamic.model.ImageEntity;
import cn.jhz.learn.community_dynamic.model.UserEntity;
import cn.jhz.learn.community_dynamic.security.helper.LoginHelper;

@Service
public class ImageService {
    private final ImageJpaRepository imageJpaRepository;
    private final FileManager fileManager;

    @Autowired
    public ImageService(ImageJpaRepository imageJpaRepository, FileManager fileManager) {
	super();
	this.imageJpaRepository = imageJpaRepository;
	this.fileManager = fileManager;
    }

    /**
     * 1.读取所有文件 2.同时创建数据库实体 3.通过id+fileName获取objectName
     * 
     * @param files
     */
    public List<ImageEntity> batchUpload(List<MultipartFile> files) {
	List<ImageEntity> entities = new ArrayList<>();
	byte[] content;
	ImageEntity entity;
	UserEntity user = (UserEntity) LoginHelper.getLoginUser().get();
	Date createTimeDate = new Date();
	try {
	    for (MultipartFile file : files) {
		content = file.getBytes();
		entity = ImageEntity.builder().createTime(createTimeDate).url("undefine").user(user).build();
		this.imageJpaRepository.saveAndFlush(entity);

		entity.setUrl(
			this.fileManager.simpleByteUpload(content, entity.getId() + "/" + file.getOriginalFilename()));
		this.imageJpaRepository.flush();
		entities.add(entity);
	    }
	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return entities;
    }

    /**
     * 1.读取所有文件 2.同时创建数据库实体 3.通过id+fileName获取objectName
     * 
     * @param files
     */
    public ImageEntity upload(MultipartFile file) {
	ImageEntity entity = null;
	UserEntity user = (UserEntity) LoginHelper.getLoginUser().get();
	Date createTimeDate = new Date();
	try {
	    entity = ImageEntity.builder().createTime(createTimeDate).url("undefine").user(user).build();
	    this.imageJpaRepository.saveAndFlush(entity);

	    entity.setUrl(
		    this.fileManager.simpleByteUpload(file.getBytes(), entity.getId() + "/" + file.getOriginalFilename()));
	    this.imageJpaRepository.flush();

	} catch (IOException e) {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
	}

	return entity;
    }
    
    public ImageEntity review(ImageEntity entity) {
	// TODO 校验
	return entity;
    }
    
    @Cacheable(cacheNames = "reviewImage", key = "#key", unless = "#result == null")
    public Optional<List<Integer>> cacheReview(String key, List<Integer> cache) {
	return Optional.ofNullable(cache);
    }
    
    @CacheEvict(cacheNames="reviewImage", allEntries=true)
    public void clearReviewCache(String key){}
    
    @CachePut(cacheNames = "reviewImage", key = "#key", unless = "#result == null")
    public Optional<List<Integer>> putReviewCache(String key, List<Integer> cache){
        return Optional.ofNullable(cache);
    }

    @Cacheable(cacheNames = "reviewImage", unless = "#result == null")
    public Optional<List<Integer>> getReviewCache(String key){
        return Optional.empty();
    }
}
