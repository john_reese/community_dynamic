package cn.jhz.learn.community_dynamic.wbms.controller.api;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.jhz.learn.community_dynamic.common.bean.JsonData;
import cn.jhz.learn.community_dynamic.common.exception.RequestException;
import cn.jhz.learn.community_dynamic.common.util.VerifyHelper;
import cn.jhz.learn.community_dynamic.dao.PermissionJpaRepository;
import cn.jhz.learn.community_dynamic.manager.PermissionManager;
import cn.jhz.learn.community_dynamic.vo.app.AclView;

@RestController
@RequestMapping("api/")
public class PermissionController {
    private final PermissionJpaRepository permissionJpaRepository;
    private final PermissionManager permissionManager;

    @Autowired
    public PermissionController(PermissionJpaRepository permissionJpaRepository, PermissionManager permissionManager) {
	super();
	this.permissionJpaRepository = permissionJpaRepository;
	this.permissionManager = permissionManager;
    }
    
    @GetMapping("auth/permission/list")
    @PreAuthorize("hasAuthority('permission::get_list')")
    public JsonData getList() {
	// 缺少权限验证
	return JsonData.newSuccessInstance(this.permissionJpaRepository.findAll().parallelStream().map(AclView.adpt).collect(Collectors.toList()));
    }
    
    @PostMapping("auth/permission/add")
    @PreAuthorize("hasAuthority('permission::add')")
    public JsonData add(@RequestBody AclView permission) {
	/**
	 * 验证外键是否有效 验证name是否有效
	 */	
	VerifyHelper.verify(permission, (c) -> {
	    switch (c) {
	    case PARENT_ID:
		if (permission.getParentId() != 0) {
		    if (!this.permissionJpaRepository.existsById(permission.getParentId())) {
			throw new RequestException("模块id不存在!");
		    }
		}
		break;
	    case NAME:
		if (this.permissionJpaRepository.existsByname(permission.getName())) {
		    throw new RequestException("存在相同名字权限!");
		}
		break;
	    case TYPE:
	    case RESOURCE:
	    case STATUS:
	    case SEQ:
		break;
	    default:
		VerifyHelper.defaultAction.accept(c);
	    }
	});
	
	return JsonData.newSuccessInstance(AclView.adpt.apply(this.permissionManager.addInstance(this.permissionManager.newInstanceByPermission(permission))));

    }
    
    @PatchMapping("auth/permission/info")
    @PreAuthorize("hasAuthority('permission::patch')")
    public JsonData patch(@RequestBody AclView permission) {
	
	VerifyHelper.verify(permission, (c) -> {
	    switch (c) {
	    case ID:
		if(!this.permissionJpaRepository.existsById(permission.getId())) {
		    throw new RequestException("权限不存在!");
		}
		break;
	    case PARENT_ID:
		if (permission.getParentId() != 0) {
		    if (!this.permissionJpaRepository.existsById(permission.getParentId())) {
			throw new RequestException("模块id不存在!");
		    }
		}
		break;
	    case NAME:
		if (this.permissionJpaRepository.existsByname(permission.getName())) {
		    throw new RequestException("存在相同名字权限!");
		}
		break;
	    case TYPE:
	    case RESOURCE:
	    case STATUS:
	    case SEQ:
		break;
	    default:
		VerifyHelper.defaultAction.accept(c);
	    }
	});
	
	return JsonData.newSuccessInstance(AclView.adpt.apply(this.permissionManager.updataByPermission(permission)));
    }
    
}
