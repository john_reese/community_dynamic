package cn.jhz.learn.community_dynamic.wbms.controller.advice;

import cn.jhz.learn.community_dynamic.common.GLOBAL;
import cn.jhz.learn.community_dynamic.common.bean.JsonData;
import cn.jhz.learn.community_dynamic.common.exception.*;

import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

//    @ExceptionHandler( value = {MethodArgumentNotValidException.class})
//    public JsonData validationFailedHandle(MethodArgumentNotValidException exception){
//        return JsonData.newFailInstance(GLOBAL.STATUS_CODE.BAD_REQUEST,exception.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(",")));
//    }
//
//    @ExceptionHandler( value = {SMSException.class})
//    public JsonData readFailedHandle(SMSException exception){
//        return JsonData.newFailInstance(GLOBAL.STATUS_CODE.REPEATED_REQUEST,exception.getMessage());
//    }


    @ExceptionHandler( value = {VerificationCodeErrorException.class, VerificationCodeNotExistException.class, VerificationCodeAcquisitionExceptions.class})
    public JsonData verificationCodeFaileHandle(RuntimeException exception){
        if(exception instanceof VerificationCodeErrorException){
            return JsonData.newFailInstance(GLOBAL.STATUS_CODE.CODE_ERROR, exception.getMessage() == null ? "验证码错误!" : exception.getMessage());
        }else if(exception instanceof  VerificationCodeNotExistException){
            return JsonData.newFailInstance(GLOBAL.STATUS_CODE.CODE_EXPIRED, exception.getMessage() == null ? "请重新获取验证码!" : exception.getMessage());
        }
        else{
            return JsonData.newFailInstance(GLOBAL.STATUS_CODE.REPEATED_REQUEST, exception.getMessage() == null ? "验证码已发送!" : exception.getMessage());
        }
    }

    /**
     * 处理登录服务异常
     * @param exception
     * @return
     */
//    @ExceptionHandler(value = {
//            AccountFormatErrorException.class,
//            AccountNotFoundException.class,
//            AccountPasswordErrorException.class,
//            AccountStatusException.class})
//    public JsonData loginFailedHandle(RuntimeException exception){
//        if(exception instanceof AccountFormatErrorException){
//            return JsonData.newFailInstance(GLOBAL.STATUS_CODE.FORMAT_ERROR, exception.getMessage() == null ? "账号格式错误!" : exception.getMessage());
//        }else if(exception instanceof AccountNotFoundException){
//            return JsonData.newFailInstance(GLOBAL.STATUS_CODE.ACCOUNT_ERROR, exception.getMessage() == null ? "账号或密码错误!" : exception.getMessage());
//        }else if(exception instanceof AccountPasswordErrorException){
//            return JsonData.newFailInstance(GLOBAL.STATUS_CODE.ACCOUNT_ERROR, exception.getMessage() == null ? "账号或密码错误!" : exception.getMessage());
//        }else{
//            return JsonData.newFailInstance(GLOBAL.STATUS_CODE.STATUS_DISABLED, exception.getMessage() == null ? "账号被停用!" : exception.getMessage());
//        }
//    }
}
