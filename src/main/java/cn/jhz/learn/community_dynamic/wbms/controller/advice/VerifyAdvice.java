package cn.jhz.learn.community_dynamic.wbms.controller.advice;


import java.util.stream.Collectors;

import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import cn.jhz.learn.community_dynamic.common.GLOBAL;
import cn.jhz.learn.community_dynamic.common.bean.JsonData;
import cn.jhz.learn.community_dynamic.common.exception.RequestException;
import cn.jhz.learn.community_dynamic.common.exception.SMSException;

@RestControllerAdvice
public class VerifyAdvice {
    
    @ExceptionHandler( value = {RequestException.class, MethodArgumentNotValidException.class})
    public JsonData validationFailedHandle(MethodArgumentNotValidException exception){
        return JsonData.newFailInstance(GLOBAL.STATUS_CODE.BAD_REQUEST,exception.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(",")));
    }

    @ExceptionHandler( value = {SMSException.class})
    public JsonData readFailedHandle(SMSException exception){
        return JsonData.newFailInstance(GLOBAL.STATUS_CODE.REPEATED_REQUEST,exception.getMessage());
    }
    
//    @ExceptionHandler( value = {Exception.class})
//    public JsonData f(Exception exception){
//	exception.printStackTrace();
//        return JsonData.newFailedInstance(exception);
//    }
}
