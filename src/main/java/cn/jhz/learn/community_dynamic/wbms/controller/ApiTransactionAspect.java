package cn.jhz.learn.community_dynamic.wbms.controller;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import cn.jhz.learn.community_dynamic.common.util.RequestHelper;

@Aspect
@Order(0)
@Component
public class ApiTransactionAspect {
    @Pointcut(value = "execution(* cn.jhz.learn.community_dynamic.controller.api.*.*(..))")
    public void all() {}
    
    @After("all()")
    public void clear() {
	RequestHelper.clear();
    }
}
