package cn.jhz.learn.community_dynamic.wbms.controller.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.jhz.learn.community_dynamic.common.bean.JsonData;
import cn.jhz.learn.community_dynamic.common.exception.RequestException;
import cn.jhz.learn.community_dynamic.dao.PostJpaRepository;
import cn.jhz.learn.community_dynamic.dao.TopicClassJpaRepository;
import cn.jhz.learn.community_dynamic.dao.TopicJpaRepository;
import cn.jhz.learn.community_dynamic.dao.UserJpaRepository;
import cn.jhz.learn.community_dynamic.service.StatsService;


@RestController
@RequestMapping("api/")
public class AuthStatsController {
    
    private final UserJpaRepository userJpaRepository;
    private final TopicClassJpaRepository topicClassJpaRepository;
    private final TopicJpaRepository topicJpaRepository;
    private final PostJpaRepository postJpaRepository;
    private final StatsService statsService;
    
    
    @Autowired
    public AuthStatsController(UserJpaRepository userJpaRepository, TopicClassJpaRepository topicClassJpaRepository, TopicJpaRepository topicJpaRepository, PostJpaRepository postJpaRepository, StatsService statsService) {
	this.userJpaRepository = userJpaRepository;
	this.topicClassJpaRepository = topicClassJpaRepository;
	this.topicJpaRepository = topicJpaRepository;
	this.postJpaRepository = postJpaRepository;
	this.statsService = statsService;
    }
    
//    @GetMapping()
//    @PreAuthorize(value = "")
//    public JsonData countAllUser() {
//	
//	return JsonData.newSuccessInstance();
//    }
//    
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countAllPost() {
//	
//	return JsonData.newSuccessInstance();
//    }
//
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countAllPostClass() {
//	
//	return JsonData.newSuccessInstance();
//    }
    
    /**
     * & 统计所有文章
     * type 线图 饼图 柱图
     * @param type
     * @return
     */
    @GetMapping("auth/wbms/topic/class/all/count")
    @PreAuthorize(value = "hasAuthority('count::count_all_topic_class')")
    public JsonData countAllTopicClass(@RequestParam("type") Byte type) {
	if(type.equals((byte) 0)) {
	    return JsonData.newSuccessInstance(this.statsService.countAllLinePostCount());
	}else if(type.equals((byte) 1)) {
	    return JsonData.newSuccessInstance(this.statsService.countAllPiePostCount());
	}else if(type.equals((byte) 2)){
	    return JsonData.newSuccessInstance();
	}else {
	    throw new RequestException("类型错误!");
	}
    }
    
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countAllTopic() {
//	
//	return JsonData.newSuccessInstance();
//    }
//    
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countAllComment() {
//	
//	return JsonData.newSuccessInstance();
//    }
//    
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countUser() {
//	
//	return JsonData.newSuccessInstance();
//    }
//    
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countPost() {
//	
//	return JsonData.newSuccessInstance();
//    }
//    
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countPostClass() {
//	
//	return JsonData.newSuccessInstance();
//    }
//    
//    
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countTopicClass() {
//	
//	return JsonData.newSuccessInstance();
//    }
//    
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countTopic() {
//	
//	return JsonData.newSuccessInstance();
//    }
//    
//    @GetMapping
//    @PreAuthorize(value = "")
//    public JsonData countComment() {
//	
//	return JsonData.newSuccessInstance();
//    }
//    
}
