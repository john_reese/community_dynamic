package cn.jhz.learn.community_dynamic.qo;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@AllArgsConstructor
@Builder
public class PostSubmit {
    /**
     * request parameter 
     */
    private String text;
    // TODO 修改前端相应代码
    @JsonProperty("imglist")
    private List<Integer> imgIdList;
    @JsonProperty("topic_id")
    private Integer topicId;
    // TODO 修改前端对应代码
    @JsonProperty("isopen")
    private Byte isOpen;
    @JsonProperty("post_class_id")
    private Integer postClassId;
    
    /**
     * other column
     */
    @JsonIgnore
    private final List<ParamType> paramTypes;
    
    public enum ParamType {
	img_id_list,
	is_open,
	topic_id,
	post_class_id,
	text;
    }

    public PostSubmit() {
	super();
	// TODO Auto-generated constructor stub
	paramTypes = new ArrayList<>();
    }
    
    public void setImgIdList(List<Integer> imgIdList) {
        this.imgIdList = imgIdList;
        this.paramTypes.add(ParamType.img_id_list);
    }

    public void setText(String text) {
        this.text = text;
        this.paramTypes.add(ParamType.text);
    }

    public void setIsOpen(Byte isOpen) {
        this.isOpen = isOpen;
        this.paramTypes.add(ParamType.is_open);
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
        this.paramTypes.add(ParamType.topic_id);
    }

    public void setPostClassId(Integer postClassId) {
        this.postClassId = postClassId;
        this.paramTypes.add(ParamType.post_class_id);
    }
}
