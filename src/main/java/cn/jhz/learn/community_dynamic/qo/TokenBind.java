package cn.jhz.learn.community_dynamic.qo;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import cn.jhz.learn.community_dynamic.security.validation.RegexpRules;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TokenBind {
    @Pattern(regexp = RegexpRules.phone, message = "无效手机号码!")
    @NotBlank(groups = { RegexpRules.Phone.class }, message = "手机号码不能为空")
    private String phone;
    @Email(groups = { RegexpRules.Email.class }, message = "无效邮箱!")
    @NotBlank(groups = { RegexpRules.Email.class }, message = "邮箱不能为空!")
    private String email;
    @Pattern(regexp = RegexpRules.verificationCode, message = "无效验证码!")
    @NotBlank(message = "验证码不能为空!")
    private String code;
}
