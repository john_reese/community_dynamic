package cn.jhz.learn.community_dynamic.qo;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserInfo {
    private String name;
    private Byte sex;
    private Byte qg;
    private String job;
    private Date birthday;
    private String address;
}