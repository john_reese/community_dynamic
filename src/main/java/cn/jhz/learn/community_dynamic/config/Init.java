package cn.jhz.learn.community_dynamic.config;

import java.util.LinkedList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import cn.jhz.learn.community_dynamic.CommunityDynamicApplication;
import cn.jhz.learn.community_dynamic.service.CommentService;
import cn.jhz.learn.community_dynamic.service.ImageService;
import cn.jhz.learn.community_dynamic.service.PostService;

public class Init implements ApplicationListener<ContextRefreshedEvent> {

    private final PostService postService;
    private final CommentService commentService;
    private final ImageService imageService;
    
    @Autowired
    public Init(PostService postService,CommentService commentService, ImageService imageService) {
	super();
	this.postService = postService;
	this.commentService = commentService;
	this.imageService = imageService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
	// TODO Auto-generated method stub
	this.postService.cacheReview(CommunityDynamicApplication.REVIEW_CACHE, new LinkedList<>());
	this.commentService.cacheReview(CommunityDynamicApplication.REVIEW_CACHE, new LinkedList<>());
	this.imageService.cacheReview(CommunityDynamicApplication.REVIEW_CACHE, new LinkedList<>());
    }

}
