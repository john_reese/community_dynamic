package cn.jhz.learn.community_dynamic.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

import cn.jhz.learn.community_dynamic.app.websocket.ConnectHandshakeInterceptor;
import cn.jhz.learn.community_dynamic.app.websocket.WebsocketHandler;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
    
    private final ConnectHandshakeInterceptor connectHandshakeInterceptor;
    private final WebsocketHandler msgHandler;
    
    @Autowired
    public WebSocketConfig(ConnectHandshakeInterceptor connectHandshakeInterceptor, WebsocketHandler msgHandler) {
	super();
	this.connectHandshakeInterceptor = connectHandshakeInterceptor;
	this.msgHandler = msgHandler;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

	// 前台 可以使用websocket环境
	registry.addHandler(msgHandler, "/websocket").setAllowedOrigins("*").addInterceptors(connectHandshakeInterceptor);
    }

}
