package cn.jhz.learn.community_dynamic.vo.app;

import java.util.HashMap;
import java.util.Map;

import cn.jhz.learn.community_dynamic.model.ImageEntity;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ImageView {

    private String url;
    Map<String, Object> pivot;

    public ImageView() {
	super();
	// TODO Auto-generated constructor stub
	pivot = new HashMap<>();
	
    }
    
    public static ImageView postAdpt(ImageEntity entity, Integer postId) {
	ImageView view = new ImageView();
	view.setUrl(entity.getUrl());
	view.pivot.put("post_id", postId);
	view.pivot.put("id", entity.getId());
	view.pivot.put("image_id", entity.getId());
	view.pivot.put("create_time", entity.getCreateTime());
	return view;
    }
   
}
