package cn.jhz.learn.community_dynamic.vo.app;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MsgView {
    @JsonProperty("to_id")
    String toId;
    @JsonProperty("from_id")
    String fromId;
    @JsonProperty("from_userpic")
    String fromUserPic;
    @JsonProperty("from_username")
    String fromUsername;
    String data;
    Boolean type;
}
