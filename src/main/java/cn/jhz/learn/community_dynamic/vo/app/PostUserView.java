package cn.jhz.learn.community_dynamic.vo.app;

import java.util.List;
import java.util.stream.Collectors;

import cn.jhz.learn.community_dynamic.model.UserEntity;
import cn.jhz.learn.community_dynamic.model.UserInfoEntity;
import lombok.Getter;

@Getter
public class PostUserView {
    private Integer id;
    private String username;
    private String userpic;
    private List<?> fen;
    private UserInfoEntity userinfo;
    
    public static PostUserView adpt(UserEntity entity) {
	// TODO 缺fen;
	PostUserView view = new PostUserView();
	view.id = entity.getId();
	view.username = entity.getUsername();
	view.userpic = entity.getUserpic();
	view.userinfo = entity.getUserInfo();
	view.fen = entity.getFolloweds().stream().map(PostUserView::fensAdpt).collect(Collectors.toList());
	
	return view;
    }
    
    public static PostUserView fensAdpt(UserEntity entity) {
	// TODO 缺fen;
	PostUserView view = new PostUserView();
	view.id = entity.getId();
	view.username = entity.getUsername();
	view.userpic = entity.getUserpic();
	view.userinfo = entity.getUserInfo();
	
	
	return view;
    }
}
