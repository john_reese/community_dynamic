package cn.jhz.learn.community_dynamic.vo.app;

import java.io.Serializable;
import java.util.function.Function;

import com.fasterxml.jackson.annotation.JsonProperty;

import cn.jhz.learn.community_dynamic.common.util.RequestHelper;
import cn.jhz.learn.community_dynamic.model.AclEntity;
import cn.jhz.learn.community_dynamic.vo.wbms.ColumnType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AclView implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -7576939649357919120L;
    private Integer id;
    private String name;
    private String type;
    private String resource;
    @JsonProperty("parent_id")
    private Integer parentId;
    private Byte status;
    private Byte seq;
    
    public static final Function<AclEntity, AclView> adpt = 
	    acl -> AclView.builder()
	    	.id(acl.getId())
	    	.name(acl.getName())
	    	.type(acl.getType())
	    	.status(acl.getStatus())
	    	.resource(acl.getResource())
	    	.parentId(acl.getParentId())
	    	.seq(acl.getSeq())
	    	.build();

    public void setId(Integer id) {
        this.id = id;
        RequestHelper.addColumnType(ColumnType.ID);
    }

    public void setName(String name) {
        this.name = name;
        RequestHelper.addColumnType(ColumnType.NAME);
    }

    public void setType(String type) {
        this.type = type;
        RequestHelper.addColumnType(ColumnType.TYPE);
    }

    public void setResource(String resource) {
        this.resource = resource;
        RequestHelper.addColumnType(ColumnType.RESOURCE);
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
        RequestHelper.addColumnType(ColumnType.PARENT_ID);
    }
    
    public void setStatus(Byte status) {
        this.status = status;
        RequestHelper.addColumnType(ColumnType.STATUS);
    }
    public void setSeq(Byte seq) {
	this.seq = seq;
	RequestHelper.addColumnType(ColumnType.SEQ);
    }
}
