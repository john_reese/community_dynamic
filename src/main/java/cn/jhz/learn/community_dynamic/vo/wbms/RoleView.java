package cn.jhz.learn.community_dynamic.vo.wbms;

import java.io.Serializable;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import cn.jhz.learn.community_dynamic.common.util.RequestHelper;
import cn.jhz.learn.community_dynamic.model.AclEntity;
import cn.jhz.learn.community_dynamic.model.RoleEntity;
import cn.jhz.learn.community_dynamic.security.validation.RegexpRules;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;



@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
public class RoleView implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6640783014035047098L;
    
    @Max(value = 1)
    @NotNull(groups = {RegexpRules.Patch.class}, message = "id不能为空!")
    private Integer id;
    private String name;
    private Set<Integer> permissions;
    @Range(min=0, max=1, message = "无效状态!") 
    private Byte status;
    
    
    public static final Function<RoleEntity, RoleView> adpt = role -> RoleView.builder().id(role.getId())
	    .name(role.getName()).status(role.getStatus())
	    .permissions(
		    role.getAcls().parallelStream().map(AclEntity::getId).collect(Collectors.toSet()))
	    .build();


    public void setId(Integer id) {
        this.id = id;
        RequestHelper.addColumnType(ColumnType.ID);
    }


    public void setName(String name) {
        this.name = name;
        RequestHelper.addColumnType(ColumnType.NAME);
    }


    public void setPermissions(Set<Integer> permissions) {
        this.permissions = permissions;
        RequestHelper.addColumnType(ColumnType.PERMISSIONS);
    }


    public void setStatus(Byte status) {
        this.status = status;
        RequestHelper.addColumnType(ColumnType.STATUS);
    }
}
