package cn.jhz.learn.community_dynamic.vo.wbms;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cn.jhz.learn.community_dynamic.model.ImageEntity;
import cn.jhz.learn.community_dynamic.model.PostClassEntity;
import cn.jhz.learn.community_dynamic.model.PostEntity;
import cn.jhz.learn.community_dynamic.model.TopicEntity;

import lombok.Getter;

@Getter
public class PostView {
    private Integer id;
    @JsonProperty("class")
    private ClassView postClass;
    private List<ClassView> topics; 
    private String title;
    private Byte type;
    private Integer shareId;
    private Long author;
    private Integer shareCount;
    private Byte isOpen;
    private Byte status;
    private Date createTime;
    private String path;
    private String content;
    private List<Images> images;
    
    @Getter
    private class Images {
	private Integer id;
	private String url;
	private Images(ImageEntity entity) {
	    super();
	    this.id = entity.getId();
	    this.url = entity.getUrl();
	}
    }
    @Getter
    private class ClassView {
	private Integer id;
	private String title;
	private ClassView(TopicEntity entity) {
	    super();
	    this.id = entity.getId();
	    this.title = entity.getTitle();
	}
	private ClassView(PostClassEntity entity) {
	    super();
	    this.id = entity.getId();
	    this.title = entity.getClassName();
	}
    }
    
    static public PostView adpt(PostEntity entity) {
	PostView view = new PostView();
	
	view.id = entity.getId();
	view.author = entity.getUser().getLoginId();
	view.content = entity.getContent();
	view.createTime = entity.getCreateTime();
	view.isOpen = entity.getIsOpen();
	view.status = entity.getStatus();
	view.type = entity.getType();
	view.shareCount = entity.getShareCount();
	if(entity.getShare() != null) {
	    view.shareId = entity.getShare().getId();
	}
	view.title = entity.getTitle();
	view.path = entity.getPath();
	
	view.topics = entity.getTopics().stream().map(topicEntity-> view.new ClassView(topicEntity)).collect(Collectors.toList());
	view.postClass = view.new ClassView(entity.getPostClass());
	view.images = entity.getImages().stream().map(imageEntity-> view.new Images(imageEntity)).collect(Collectors.toList());
	
	return view;
    }
}
