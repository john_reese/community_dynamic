package cn.jhz.learn.community_dynamic.vo.app;

import com.fasterxml.jackson.annotation.JsonProperty;

import cn.jhz.learn.community_dynamic.model.PostClassEntity;
import cn.jhz.learn.community_dynamic.model.TopicClassEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ClassView {
    private Integer id;
    @JsonProperty("classname")
    private String className;
    
    public static ClassView adpt(PostClassEntity classEntity) {
	return new ClassView(classEntity.getId(), classEntity.getClassName());
    }
    
    public static ClassView adpt(TopicClassEntity classEntity) {
	return new ClassView(classEntity.getId(), classEntity.getClassName());
    }
}
