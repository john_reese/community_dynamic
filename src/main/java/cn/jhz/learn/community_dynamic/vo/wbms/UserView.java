package cn.jhz.learn.community_dynamic.vo.wbms;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;


import cn.jhz.learn.community_dynamic.common.util.RequestHelper;
import cn.jhz.learn.community_dynamic.model.RoleEntity;
import cn.jhz.learn.community_dynamic.model.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Builder
public class UserView implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7497706732754537273L;
    
    private String email;
    private Long loginId;
    private String password;
    private String mobile;
    private Date registrationTime;
    private Byte status;
    private String username;
    private String userpic;
    private Set<Integer> roleIds;
    
    public static final Function<UserEntity, UserView> adpt = user-> UserView.builder()
		.email(user.getEmail())
		.loginId(user.getLoginId())
		.mobile(user.getPhone())
		.registrationTime(user.getRegistrationTime())
		.status(user.getStatus())
		.username(user.getUsername())
		.userpic(user.getUserpic())
		.roleIds(user.getRoles().parallelStream().map(RoleEntity::getId).collect(Collectors.toSet()))
		.build();
    
    

    public void setEmail(String email) {
        this.email = email;
        RequestHelper.addColumnType(ColumnType.EMAIL);
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
        RequestHelper.addColumnType(ColumnType.LOGIN_ID);
    }

    public void setPassword(String password) {
        this.password = password;
        RequestHelper.addColumnType(ColumnType.PASSWORD);
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
        RequestHelper.addColumnType(ColumnType.MOBILE);
    }

    public void setStatus(Byte status) {
        this.status = status;
        RequestHelper.addColumnType(ColumnType.STATUS);
    }

    public void setUsername(String username) {
        this.username = username;
        RequestHelper.addColumnType(ColumnType.USERNAME);
    }

    public void setUserpic(String userpic) {
        this.userpic = userpic;
        RequestHelper.addColumnType(ColumnType.USERPIC);
    }

    public void setRoleIds(Set<Integer> roleIds) {
        this.roleIds = roleIds;
        RequestHelper.addColumnType(ColumnType.ROLES);
    }
}
