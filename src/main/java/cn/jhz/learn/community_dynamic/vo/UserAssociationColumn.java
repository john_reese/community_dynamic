package cn.jhz.learn.community_dynamic.vo;

public enum UserAssociationColumn {
    roles,
    userBinds,
    supports,
    comments,
    images,
    posts,
    follows,
    followeds,
    feedbacks,
    black,
    blacked,
    userInfo;
}
