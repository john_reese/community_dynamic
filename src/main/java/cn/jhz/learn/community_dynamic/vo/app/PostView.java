package cn.jhz.learn.community_dynamic.vo.app;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.BeanUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

import cn.jhz.learn.community_dynamic.model.PostEntity;
import cn.jhz.learn.community_dynamic.model.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostView implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -5002257604869157582L;

    /**
     * to copy parameter
     */
    private String title; // copy
    private String titlepic; // copy
    @JsonProperty("sharenum")
    private Integer shareCount; // copy
    @JsonProperty("create_time")
    private Date createTime; // copy
    @JsonProperty("user_id")
    private Long userId; // set
    private Integer id; // copy
    private Integer ding_count; // count
    private Integer cai_count; // count 
    private Integer comment_count; // count 
    private List<Support> support; // set

    private Map<String, Object> user; // set username;userpic;fen;  
    
    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public class Support {
	private Integer user_id;
	private Byte type;
    }
    
    public static PostView classPageView(PostEntity entity, Integer dingCount, Integer caiCount, Integer commentCount) {
	/**
	 * 1.复制相同属性
	 * 2.手动赋值属性
	 * 3.查询统计属性
	 */
	PostView view = new PostView();
	BeanUtils.copyProperties(entity, view);
	
	view.userId = entity.getUser().getLoginId();
	view.support = new ArrayList<>();
	view.support.addAll(entity.getSupportUsers().stream().map(e -> view.new Support(e.getId(), (byte) 0)).collect(Collectors.toList()));
	view.support.addAll(entity.getUnSupportUsers().stream().map(e -> view.new Support(e.getId(), (byte) 0)).collect(Collectors.toList()));
	
	view.user =  new HashMap<>();
	view.user.put("username", entity.getUser().getUsername());
	view.user.put("userpic", entity.getUser().getUserpic());
	view.user.put("fens", new ArrayList<>(entity.getUser().getFolloweds().stream().map(UserEntity::getId).collect(Collectors.toList())));
	
	view.ding_count = dingCount;
	view.cai_count = caiCount;
	view.comment_count = commentCount;
	
	return view;
    }
    

    

//    public static PostView postDetailAdpt(PostEntity entity) {
//	PostView view = new PostView();
//	PostView share = new PostView();
//
//	BeanUtils.copyProperties(entity, view);
//	view.setPostClassId(entity.getPostClass().getId());
//	view.setImages(entity.getImages().stream().map(image -> ImageView.adpt(image, view.getId())).collect(Collectors.toList()));
//	// TODO 缺少fen
//	view.setUser(view.new UserView(entity.getUser().getId(), entity.getUser().getUsername(),
//		entity.getUser().getUserpic(), null, entity.getUser().getUserInfo()));
//	PostEntity shareEntity1 = entity.getShare();
//	if (ObjectUtils.isNotEmpty(shareEntity1)) {
//	    BeanUtils.copyProperties(shareEntity1, share, "content");
//	    PostEntity shareEntity2 = shareEntity1.getShare();
//	    if (ObjectUtils.isNotEmpty(shareEntity2)) {
//		share.setShareId(shareEntity2.getId());
//	    }
//	    // TODO 后期修改为梗概
//	    share.setContent(shareEntity2.getContent());
//	}
//	view.setShare(share);
//
//	return view;
//    }
//    
//    public static PostView topicPostDetailAdpt(PostEntity entity) {
//	PostView view = PostView.postDetailAdpt(entity);
//	
//	return view;
//    }
}
