/**
 * 
 */
package cn.jhz.learn.community_dynamic.vo.wbms;


/**
 * @author machine005
 *
 */
public enum ColumnType {
//    EMAIL("email"), 
//    LOGIN_ID("loginId"), 
//    PASSWORD("password"), 
//    MOBILE("mobile"), 
//    STATUS("status"), 
//    USERNAME("username"),
//    USERPIC("userpic"), 
//    ROLES("roles"),
//    NAME("name"), 
//    TYPE("type"), 
//    RESOURCE("resource"), 
//    PARENT_ID("parent_id"),
//    PERMISSIONS("permissions"),
//    ERROR("error");
    ID,
    EMAIL, 
    LOGIN_ID, 
    PASSWORD, 
    MOBILE, 
    STATUS, 
    USERNAME,
    USERPIC, 
    ROLES,
    NAME, 
    TYPE, 
    RESOURCE, 
    PARENT_ID,
    PERMISSIONS,
    ERROR,
    SEQ;

//    private final String value;
//
//    ColumnType(String value) {
//	this.value = value;
//    }

    // 根据value返回枚举类型,主要在switch中使用
//    public static ColumnType getByValue(String value) {
//	for (ColumnType code : values()) {
//	    if (code.value == value) {
//		return code;
//	    }
//	}
//	return ERROR;
//    }
}
