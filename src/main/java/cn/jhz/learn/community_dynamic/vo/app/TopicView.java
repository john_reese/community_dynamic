package cn.jhz.learn.community_dynamic.vo.app;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import cn.jhz.learn.community_dynamic.model.TopicEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TopicView {
    private Integer id;
    private String desc;
    private String title;
    private String titlepic;
    private Byte type;
    @JsonProperty("create_time")
    private Date createTime;
    @JsonProperty("topic_class_id")
    private Integer topicClassId;
    @JsonProperty("post_count")
    private Integer postCount;
    @JsonProperty("todaypost_count")
    private Integer todaypostCount;

    public static TopicView adpt(TopicEntity entity) {
	return TopicView.builder().id(entity.getId()).topicClassId(entity.getTopicClass().getId())
		.desc(entity.getDesc()).title(entity.getTitle()).titlepic(entity.getTitlepic()).type(entity.getType())
		.createTime(entity.getCreateTime()).build();

    }

}
