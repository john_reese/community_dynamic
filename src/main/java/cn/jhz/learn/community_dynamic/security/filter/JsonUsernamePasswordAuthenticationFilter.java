package cn.jhz.learn.community_dynamic.security.filter;

import cn.jhz.learn.community_dynamic.security.AuthenticationParameterException;
import cn.jhz.learn.community_dynamic.security.helper.LoginHelper;
import cn.jhz.learn.community_dynamic.security.validation.RegexpRules;
import cn.jhz.learn.community_dynamic.security.model.AccountType;
import cn.jhz.learn.community_dynamic.security.model.Login;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;
import org.springframework.validation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Component
public class JsonUsernamePasswordAuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final SmartValidator smartValidator;
    private final ObjectMapper mapper;

    @Autowired
    public JsonUsernamePasswordAuthenticationFilter(@Lazy AuthenticationManager authenticationManager, @Qualifier("jsonLoginSuccessHandler") AuthenticationSuccessHandler authenticationSuccessHandler,
            AuthenticationFailureHandler authenticationFailureHandler,
            SmartValidator smartValidator, 
            ObjectMapper mapper,
            @Qualifier("accountLoginAntPathRequestMatcher") AntPathRequestMatcher antPathRequestMatcher) {
	//拦截url为 "/api/*/user/account_login" 的POST请求
	super(antPathRequestMatcher);
	super.setAuthenticationManager(authenticationManager);
	this.setAuthenticationSuccessHandler(authenticationSuccessHandler);
	this.setAuthenticationFailureHandler(authenticationFailureHandler);
        this.smartValidator = smartValidator;
        this.mapper = mapper;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException, IOException, ServletException {
        //当Content-Type为json时尝试身份验证
	String contentType = request.getContentType().replace(" ", "");
        if(contentType.equals(MediaType.APPLICATION_JSON_VALUE) || contentType.equals("application/json;charset=utf-8")){
            //使用jackson反序列化json
            UsernamePasswordAuthenticationToken authRequest = null;
            try (InputStream is = request.getInputStream()){

                Login authenticationBean = mapper.readValue(is, Login.class);
                authRequest = this.createAuthRequest(authenticationBean);

            }catch (IOException e) {
                e.printStackTrace();
                //TODO 包装IO异常
                authRequest = new UsernamePasswordAuthenticationToken("", "");
            }
            return this.getAuthenticationManager().authenticate(authRequest);
        }
        else {
            throw new AuthenticationServiceException("Authentication type not supported: " + request.getContentType());
        }
    }

    private UsernamePasswordAuthenticationToken createAuthRequest(Login login){
        Errors errors = new DirectFieldBindingResult(login, "login");
        ValidationUtils.invokeValidator(this.smartValidator, login, errors, Login.AccountLogin.class);
        if (errors.hasErrors()) {
            throw new AuthenticationParameterException(
                    errors.getAllErrors().stream()
                            .map(ObjectError::getDefaultMessage)
                            .collect(Collectors.joining(",","","!"))
            );
        }else {
            //TODO 验证码未实现
            /*判断账号类型*/
            if (Pattern.matches(RegexpRules.username, login.getUsername())) {
        	LoginHelper.setAccountType(AccountType.USERNAME);
            } else if (Pattern.matches(RegexpRules.phone, login.getUsername())) {
                LoginHelper.setAccountType(AccountType.PHONE);
            } else if (Pattern.matches(RegexpRules.email, login.getUsername())) {
                LoginHelper.setAccountType(AccountType.EMAIL);
            }else if(Pattern.matches(RegexpRules.loginId, login.getUsername())){
        	LoginHelper.setAccountType(AccountType.LOGIN_ID);
            } else {
                /*賬號格式錯誤*/
                throw new AuthenticationParameterException("账号格式不正确!");
            }
            return new UsernamePasswordAuthenticationToken(login.getUsername(), login.getPassword());
        }
    }
    
//    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }
}
