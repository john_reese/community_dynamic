package cn.jhz.learn.community_dynamic.security.helper;

import java.util.Optional;

import cn.jhz.learn.community_dynamic.security.model.AccountType;

public class LoginHelper {
    private static final ThreadLocal<AccountType> accountType = new ThreadLocal<>();
    private static final ThreadLocal<Object> loginUser = new ThreadLocal<>();
    
    public static Optional<?> getLoginUser() {
        return Optional.ofNullable(loginUser.get());
    }

    public static void setLoginUser(Object user){
	loginUser.set(user);
    }
    
    public static AccountType getAccountType(){
        return accountType.get();
    }

    public static void setAccountType(AccountType type){
        accountType.set(type);
    }
    public static void clear() {
	accountType.remove();
	loginUser.remove();
    }
}
