package cn.jhz.learn.community_dynamic.security.model;

public enum AccountType {
    EMAIL,PHONE,USERNAME,LOGIN_ID,OPEN_ID,JWT,UNKNOW
}
