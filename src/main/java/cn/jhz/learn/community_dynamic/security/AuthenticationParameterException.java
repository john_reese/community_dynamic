package cn.jhz.learn.community_dynamic.security;

import org.springframework.security.core.AuthenticationException;

public class AuthenticationParameterException extends AuthenticationException{
    private static final long serialVersionUID = 4806265394783642099L;

    public AuthenticationParameterException(String msg, Throwable t) {
        super(msg, t);
    }

    public AuthenticationParameterException(String msg) {
        super(msg);
    }
}
