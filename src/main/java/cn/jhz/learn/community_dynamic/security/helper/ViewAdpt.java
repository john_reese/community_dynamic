package cn.jhz.learn.community_dynamic.security.helper;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonProperty;

import cn.jhz.learn.community_dynamic.model.UserEntity;
import lombok.Getter;

public final class ViewAdpt {
    
    static private final ViewAdpt adpt_view = new ViewAdpt(); 
    private ViewAdpt() {}
    
    @Getter
    public class AccountDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3087652540196123613L;
	private String email;
	private Long loginId;
	private String mobile;
	private Date registrationTime;
	private Byte status;
	private String username;
	private String userpic;
	private Set<RoleDetail> roles;

	public AccountDetail() {
	    super();
	    // TODO Auto-generated constructor stub
	}

	public AccountDetail(String email, Long loginId, String mobile, Date registrationTime, Byte status,
		String username, String userpic, Set<RoleDetail> roles) {
	    super();
	    this.email = email;
	    this.loginId = loginId;
	    this.mobile = mobile;
	    this.registrationTime = registrationTime;
	    this.status = status;
	    this.username = username;
	    this.userpic = userpic;
	    this.roles = roles;
	}

    }
    
    @Getter
    public class RoleDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5747220905402861072L;
	private Integer id;
	private String name;
	private Byte status;
	private Set<AclDetail> permissions;

	public RoleDetail() {
	    super();
	    // TODO Auto-generated constructor stub
	}

	public RoleDetail(Integer id, String name, Byte status, Set<AclDetail> permissions) {
	    super();
	    this.id = id;
	    this.name = name;
	    this.status = status;
	    this.permissions = permissions;
	}
    }
    
    @Getter
    public class AclDetail implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 125874086240311097L;
	private Integer id;
	private String name;
	private String type;
	private String resource;
	@JsonProperty("parent_id")
	private Integer parentId;
	private Byte seq;
	private Byte status;

	public AclDetail() {
	    super();
	    // TODO Auto-generated constructor stub
	}

	public AclDetail(Integer id, String name, String type, String resource, Integer parentId, Byte seq,
		Byte status) {
	    super();
	    this.id = id;
	    this.name = name;
	    this.type = type;
	    this.resource = resource;
	    this.parentId = parentId;
	    this.seq = seq;
	    this.status = status;
	}
    }
    
    static public AccountDetail newWbmsDetailsInstance(UserEntity user) {
	return ViewAdpt.adpt_view.new AccountDetail(user.getEmail(), user.getLoginId(), user.getPhone(), user.getRegistrationTime(),
		user.getStatus(), user.getUsername(), user.getUserpic(),
		user.getRoles().parallelStream().map(role -> ViewAdpt.adpt_view.new RoleDetail(role.getId(), role.getName(),
			role.getStatus(),
			role.getAcls().parallelStream()
				.map(acl -> ViewAdpt.adpt_view.new AclDetail(acl.getId(), acl.getName(), acl.getType(), acl.getResource(),
					acl.getParentId(), acl.getSeq(), acl.getStatus()))
				.collect(Collectors.toSet())))
			.collect(Collectors.toSet()));
    }
}
