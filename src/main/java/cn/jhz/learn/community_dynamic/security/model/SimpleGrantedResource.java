package cn.jhz.learn.community_dynamic.security.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

public final class SimpleGrantedResource implements GrantedAuthority {

    private static final long serialVersionUID = -6058202257006005931L;

    private final String resource;

    public SimpleGrantedResource(String resource) {
        Assert.hasText(resource, "A granted authority textual representation is required");
        this.resource = resource;
    }

    @Override
    public String getAuthority() {
        return resource;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof SimpleGrantedResource) {
            return resource.equals(((SimpleGrantedResource) obj).resource);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return this.resource.hashCode();
    }

    @Override
    public String toString() {
        return this.resource;
    }
}
