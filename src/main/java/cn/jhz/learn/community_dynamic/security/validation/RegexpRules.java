package cn.jhz.learn.community_dynamic.security.validation;

public final class RegexpRules {
    private RegexpRules(){}
    public interface Patch {}
    public interface Email {}
    public interface Phone {}
    public static final String phone = "^((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(17[013678])|(18[0,5-9]))\\d{8}$";
    public static final String email = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
    public static final String username = "^[\\w@\\$\\^!~,.\\*]{0,7}+$";
    public static final String verificationCode = "^\\d{6}$";
    public static final String loginId = "^[1-9][0-9]{8}$";
}
