package cn.jhz.learn.community_dynamic.security.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

public final class SimpleGrantedRole implements GrantedAuthority {

    private static final long serialVersionUID = 2572402541653037286L;

    private final String role;

    public SimpleGrantedRole(String role) {
        Assert.hasText(role, "A granted authority textual representation is required");
        this.role = "ROLE_" + role;
    }

    @Override
    public String getAuthority() {
        return role;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof SimpleGrantedRole) {
            return role.equals(((SimpleGrantedRole) obj).role);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return this.role.hashCode();
    }

    @Override
    public String toString() {
        return this.role;
    }
}
