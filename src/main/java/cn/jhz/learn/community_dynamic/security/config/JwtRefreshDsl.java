/**
 * 
 */
package cn.jhz.learn.community_dynamic.security.config;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.authentication.logout.LogoutFilter;

import cn.jhz.learn.community_dynamic.security.filter.JwtAuthenticationFilter;

/**
 * @author machine005
 *
 */
public class JwtRefreshDsl extends AbstractHttpConfigurer<JwtRefreshDsl, HttpSecurity> {
    private final JwtAuthenticationFilter jwtAuthenticationFilter;

    public JwtRefreshDsl(JwtAuthenticationFilter jwtAuthenticationFilter) {
        this.jwtAuthenticationFilter = jwtAuthenticationFilter;
    }

    @Override
    public void configure(HttpSecurity builder) throws Exception {
	this.jwtAuthenticationFilter.setAuthenticationManager(builder.getSharedObject(AuthenticationManager.class));
        /*将filter放到logoutFilter之前*/
        builder.addFilterBefore(postProcess(this.jwtAuthenticationFilter), LogoutFilter.class);
    }
    
    public static JwtRefreshDsl jwtRefreshDsl(JwtAuthenticationFilter jwtAuthenticationFilter) {
	return new JwtRefreshDsl(jwtAuthenticationFilter);
    }
}
