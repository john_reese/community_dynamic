package cn.jhz.learn.community_dynamic.security.model;

import cn.jhz.learn.community_dynamic.security.validation.RegexpRules;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class Login {
    public interface GetVerificationCode {}
    public interface AccountLogin {}
    public interface PhoneLogin {}

    @NotBlank(groups = {PhoneLogin.class, GetVerificationCode.class}, message = "手机号不能为空!")
    @Pattern(regexp = RegexpRules.phone, groups = {PhoneLogin.class}, message = "手机号不正确!")
    private String phone;

    @NotBlank(groups = {PhoneLogin.class})
    @Pattern(regexp = RegexpRules.verificationCode, groups = {PhoneLogin.class})
    private String code;

    @NotBlank(groups = {AccountLogin.class}, message = "用户名不能为空")
    private String username;

    @NotBlank(groups = {AccountLogin.class}, message = "密码不能为空")
    private String password;
}
