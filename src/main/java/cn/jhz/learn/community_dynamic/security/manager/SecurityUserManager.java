package cn.jhz.learn.community_dynamic.security.manager;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetails;

import cn.jhz.learn.community_dynamic.security.model.SimpleGrantedUser;

abstract public class SecurityUserManager<T> {    
    abstract public Optional<T> getUserDetailByPhone(String phone);
    abstract public Optional<T> getUserDetailByEmail(String email);
    abstract public Optional<T> getUserDetailByOtherBind(String openId, String provider);
    abstract public Optional<T> getUserDetailByLoginId(String loginId);
    abstract public Optional<T> getUserDetailByUsername(String username);
    abstract public void createUserByPhone(String phone);
    abstract public UserDetails adpt(SimpleGrantedUser obj);
}
