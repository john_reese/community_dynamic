package cn.jhz.learn.community_dynamic.app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.jhz.learn.community_dynamic.common.bean.JsonData;
import cn.jhz.learn.community_dynamic.dao.AdsenseJpaRepository;

@RestController
@RequestMapping("api/")
public class OtherController {
    
    private final AdsenseJpaRepository adsenseJpaRepository;
    
    
    @Autowired
    public OtherController(AdsenseJpaRepository adsenseJpaRepository) {
	super();
	this.adsenseJpaRepository = adsenseJpaRepository;
    }



    @GetMapping("adsense/{type}")
    public JsonData getByType(@PathVariable("type") Byte type) {
	
	return JsonData.newSuccessInstance(this.adsenseJpaRepository.findByType(type));
    }
    
    @PostMapping("auth/feedback")
    @PreAuthorize("hasAuthority('user::post_feedback')")
    public JsonData feedBack() {
	
	return JsonData.newSuccessInstance();
    }
}
