package cn.jhz.learn.community_dynamic.app.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.jhz.learn.community_dynamic.common.bean.JsonData;
import cn.jhz.learn.community_dynamic.common.util.MineCommonUtil;
import cn.jhz.learn.community_dynamic.dao.PostClassJpaRepository;
import cn.jhz.learn.community_dynamic.dao.PostJpaRepository;
import cn.jhz.learn.community_dynamic.dao.TopicClassJpaRepository;
import cn.jhz.learn.community_dynamic.dao.TopicJpaRepository;
import cn.jhz.learn.community_dynamic.vo.app.ClassView;
import cn.jhz.learn.community_dynamic.vo.app.TopicView;

@RestController
@RequestMapping("api/")
public class ThemeController {
    private final TopicClassJpaRepository topicClassJpaRepository;
    private final TopicJpaRepository topicJpaRepository;
    private final PostJpaRepository postJpaRepository;
    private final PostClassJpaRepository postClassJpaRepository;

    @Autowired
    public ThemeController(TopicClassJpaRepository topicClassJpaRepository, TopicJpaRepository topicJpaRepository,
	    PostJpaRepository postJpaRepository, PostClassJpaRepository postClassJpaRepository) {
	super();
	this.topicClassJpaRepository = topicClassJpaRepository;
	this.topicJpaRepository = topicJpaRepository;
	this.postJpaRepository = postJpaRepository;
	this.postClassJpaRepository = postClassJpaRepository;
    }

    @GetMapping("topic_class")
    public JsonData getAll() {
	Map<String, List<ClassView>> data = new HashMap<>();
	data.put("list", topicClassJpaRepository.findAllByStatus((byte) 1).parallelStream().map(ClassView::adpt)
		.collect(Collectors.toList()));
	return JsonData.newSuccessInstance("获取成功", data);
    }

    @GetMapping("topic_class/{id}/topic/{page}")
    public JsonData getPage(@PathVariable("id") Integer topicClassId, @PathVariable("page") Integer page) {
	Map<String, List<TopicView>> data = new HashMap<>();
	data.put("list", this.topicJpaRepository.findByTopicClass_id(topicClassId, PageRequest.of(page - 1, 10)).stream()
		.map(entity -> {
		    TopicView view = TopicView.adpt(entity);
		    view.setPostCount(this.postJpaRepository.countByTopics_idAndStatus(entity.getId(), (byte) 1));
		    view.setTodaypostCount(this.postJpaRepository.countByTopics_idAndStatusAndCreateTimeAfter(entity.getId(), (byte) 1, MineCommonUtil.getToday()));
		    return view;
		}).collect(Collectors.toList()));
	return JsonData.newSuccessInstance(data);
    }

    @GetMapping("hot_topic")
    public JsonData getHostTopic() {
	Map<String, List<TopicView>> data = new HashMap<>();
	data.put("list", this.topicJpaRepository.findByType((byte) 1, PageRequest.of(0, 5)).stream().map(entity -> {
	    TopicView view = TopicView.adpt(entity);
	    view.setPostCount(this.postJpaRepository.countByTopics_idAndStatus(entity.getId(), (byte) 1));
	    view.setTodaypostCount(this.postJpaRepository.countByTopics_idAndStatusAndCreateTimeAfter(entity.getId(), (byte) 1, MineCommonUtil.getToday()));
	    return view;
	}).collect(Collectors.toList()));
	return JsonData.newSuccessInstance(data);
    }
    
    @GetMapping("post_class")
    public JsonData getAllClass() {
	Map<String, List<ClassView>> data = new HashMap<>();
	data.put("list", postClassJpaRepository.findAllByStatus((byte) 1).parallelStream().map(ClassView::adpt)
		.collect(Collectors.toList()));
	return JsonData.newSuccessInstance("获取成功", data);
    }
    
    @GetMapping("search/topic")
    public JsonData searchTopic(@RequestParam("keyword") String keyword, @RequestParam("page") Integer page) {
	return JsonData.newSuccessInstance(this.topicJpaRepository.findByTitleContaining(keyword,  PageRequest.of(page - 1, 10)).stream()
		.map(entity -> {
		    TopicView view = TopicView.adpt(entity);
		    view.setPostCount(this.postJpaRepository.countByTopics_idAndStatus(entity.getId(), (byte) 1));
		    view.setTodaypostCount(this.postJpaRepository.countByTopics_idAndStatusAndCreateTimeAfter(entity.getId(), (byte) 1, MineCommonUtil.getToday()));
		    return view;
		}).collect(Collectors.toList()));
    }
}
