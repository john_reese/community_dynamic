package cn.jhz.learn.community_dynamic.common.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class JsonData {
    @Getter
    private class List<T> {
	java.util.List<T> list;
	private List(java.util.List<T> list) {
	    this.list = list;
	}
    }

    private String code;

    private String msg;

    private Object data;

    private JsonData() {
    }

    public static JsonData newSuccessInstance() {
	JsonData jsonData = new JsonData();
	jsonData.code = "1";

	return jsonData;
    }

    public static JsonData newSuccessInstance(String msg, Object data) {
	JsonData jsonData = new JsonData();

	jsonData.code = "1";
	jsonData.msg = msg;
	jsonData.data = data;

	return jsonData;
    }

    public static JsonData newSuccessInstance(Object data) {
	JsonData jsonData = new JsonData();
	jsonData.code = "1";
	jsonData.data = data;

	return jsonData;
    }

    public static <T> JsonData newSuccessInstance(java.util.List<T> list) {
	JsonData jsonData = new JsonData();
	jsonData.code = "1";
	jsonData.data = jsonData.new List<>(list);

	return jsonData;
    }

    public static JsonData newSuccessInstance(String msg) {
	JsonData jsonData = new JsonData();

	jsonData.msg = msg;

	return jsonData;
    }

    public static JsonData newFailedInstance(String code, Object data, String msg) {
	JsonData jsonData = new JsonData();

	jsonData.code = code;
	jsonData.msg = msg;
	jsonData.data = data;

	return jsonData;
    }

    public static JsonData newFailedInstance(String code, String msg) {
	JsonData jsonData = new JsonData();

	jsonData.msg = msg;
	jsonData.code = code;

	return jsonData;
    }

    public static JsonData newFailInstance(String code, String msg) {
	JsonData jsonData = new JsonData();

	jsonData.msg = msg;
	jsonData.code = code;

	return jsonData;
    }

    public static JsonData newFailedInstance(Object data) {
	JsonData jsonData = new JsonData();

	jsonData.data = data;

	return jsonData;
    }

    public static JsonData newFailedInstance(String msg) {
	JsonData jsonData = new JsonData();
	jsonData.code = "0";
	jsonData.msg = msg;

	return jsonData;
    }

    public Map<String, Object> toMap() {
	HashMap<String, Object> result = new HashMap<>();

	result.put("msg", msg);
	result.put("data", data);

	return result;
    }

}
