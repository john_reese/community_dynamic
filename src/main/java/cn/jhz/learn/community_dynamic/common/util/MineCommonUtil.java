package cn.jhz.learn.community_dynamic.common.util;

import java.util.Calendar;
import java.util.Date;

public final class MineCommonUtil {
    private MineCommonUtil() {
    }

    public static Date getToday() {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(new Date());
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	return calendar.getTime();
    }
    
    public static Date getYesterday() {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(new Date());
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.add(Calendar.DATE, -1);
	
	return calendar.getTime();
    }
    
    public static Date getTomorrow() {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(new Date());
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.add(Calendar.DATE,1);
	
	return calendar.getTime();
    }
    
    public static Date getNextYear(Date date) {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(date);
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.set(Calendar.DAY_OF_MONTH, 1);
	calendar.add(Calendar.YEAR, 1);
	
	
	return calendar.getTime();
    }
    
    public static Date getLastYear() {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(new Date());
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.add(Calendar.YEAR, -1);
	
	return calendar.getTime();
    }
    
    public static Date getCurrentYear() {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(new Date());
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.set(Calendar.MONTH, 0);
	
	return calendar.getTime();
    }
    
    public static Date setMonth(Date date, Integer month) {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(date);
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.set(Calendar.MONTH, month);
	
	return calendar.getTime();
    }
    
    public static Date setMonthEnd(Date date, Integer month) {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(date);
	calendar.set(Calendar.HOUR_OF_DAY, 23);
	calendar.set(Calendar.MINUTE, 59);
	calendar.set(Calendar.SECOND, 59);
	calendar.set(Calendar.MONTH, month);
	
	return calendar.getTime();
    }
    
    public static Date setLastMonthEnd(Date date) {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(date);
	calendar.set(Calendar.HOUR_OF_DAY, 23);
	calendar.set(Calendar.MINUTE, 59);
	calendar.set(Calendar.SECOND, 59);
	calendar.set(Calendar.DAY_OF_MONTH, 31);
	calendar.set(Calendar.MONTH, 11);
	
	return calendar.getTime();
    }
    
    public static Date getYear(Date date) {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(date);
	calendar.set(Calendar.MONTH, 0);
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.set(Calendar.DAY_OF_MONTH, 1);
	return calendar.getTime();
    }
    
    public static Date getYear(Date date, Integer next) {
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(date);
	calendar.set(Calendar.MONTH, 0);
	calendar.set(Calendar.HOUR_OF_DAY, 0);
	calendar.set(Calendar.MINUTE, 0);
	calendar.set(Calendar.SECOND, 0);
	calendar.add(Calendar.YEAR, next);
	return calendar.getTime();
    }
    
    public static int yearSubtract(Date year1, Date year2) {
	Calendar calendar1 = Calendar.getInstance();
	calendar1.setTime(year1);
	Calendar calendar2 = Calendar.getInstance();
	calendar2.setTime(year2);
	
	return calendar1.get(Calendar.YEAR) - calendar2.get(Calendar.YEAR);
    }
    
}
