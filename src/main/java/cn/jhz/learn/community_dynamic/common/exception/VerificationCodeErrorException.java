package cn.jhz.learn.community_dynamic.common.exception;

public class VerificationCodeErrorException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 2885573334639396786L;

    public VerificationCodeErrorException() {
    }

    public VerificationCodeErrorException(String message) {
        super(message);
    }

    public VerificationCodeErrorException(String message, Throwable cause) {
        super(message, cause);
    }

    public VerificationCodeErrorException(Throwable cause) {
        super(cause);
    }

    public VerificationCodeErrorException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
