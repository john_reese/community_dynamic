package cn.jhz.learn.community_dynamic.common;

public class GLOBAL {
    private GLOBAL(){}

    public static final class TOKEN_INFO {
        private TOKEN_INFO(){}

        public static final String AUTH_HEADER = "X-Authorization-With";

        public static final int EXPIRATION_TIME_MINUTES_IN_THE_FUTURE = 10;

        public static final int NOT_BEFORE_MINUTES_IN_THE_PAST = 2;

        public static final int RSA_JSON_WEB_KEY_BITS = 2048;

        public static final String RSA_JSON_WEB_KEY_ID = "machine005@cn.jhz";

        public static final int ALLOWED_CLOCK_SKEW_IN_SECONDS = 30;

        public static final String ISSUER = "MACHINE_005";
    }

    static public class STATUS_CODE {
        private STATUS_CODE(){}

        public static final String BAD_REQUEST = "4000";
        //400	Bad Request	客户端请求的语法错误，服务器无法理解
        public static final String REPEATED_REQUEST = "30001";
        //重复请求
        public static final String MSG_SENT_FAILED = "30004";
        //短信发送失败
        public static final String FORMAT_ERROR = "20000";
        //账号格式错误
        public static final String ACCOUNT_ERROR = "20000";
        //账号错误
        public static final String STATUS_DISABLED = "20001";
        //账号停用
        public static final String CODE_ERROR = "30000";
        //验证码错误
        public static final String CODE_EXPIRED = "30002";
        //验证码过期
    }

    public static final class HEADER_INFO {
        private HEADER_INFO(){}

        public static final String ACCESS_CONTROL_EXPOSE_HEADERS = "Access-Control-Expose-Headers";

        public static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-control-Allow-Origin";

        public static final String  ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";

        public static final String  ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    }
}
