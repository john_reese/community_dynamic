package cn.jhz.learn.community_dynamic.common.util;

import java.util.ArrayList;
import java.util.List;

import cn.jhz.learn.community_dynamic.vo.wbms.ColumnType;

public class RequestHelper {
    private static final ThreadLocal<List<ColumnType>> patchColumns = new ThreadLocal<List<ColumnType>>() {
	 @Override 
	 protected List<ColumnType> initialValue() {
	     
	     return new ArrayList<>();
	}
    };
    
    public static List<ColumnType> getPatchColumns() {
	return patchColumns.get();
    }
    
    public static void addColumnType(ColumnType type) {
	RequestHelper.getPatchColumns().add(type);
    }
    
    
    public static void clear() {
	RequestHelper.patchColumns.remove();
    }
    
}
