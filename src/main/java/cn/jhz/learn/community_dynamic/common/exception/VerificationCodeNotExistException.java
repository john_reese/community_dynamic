package cn.jhz.learn.community_dynamic.common.exception;

public class VerificationCodeNotExistException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 2999263799612125391L;

    public VerificationCodeNotExistException() {
    }

    public VerificationCodeNotExistException(String message) {
        super(message);
    }

    public VerificationCodeNotExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public VerificationCodeNotExistException(Throwable cause) {
        super(cause);
    }

    public VerificationCodeNotExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
