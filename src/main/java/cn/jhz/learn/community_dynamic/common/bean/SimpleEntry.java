package cn.jhz.learn.community_dynamic.common.bean;

import java.util.Optional;

public class SimpleEntry<K, V> {
	
	private Optional<K> key;
	private Optional<V> value;
	
	public SimpleEntry(K key, V value) {
		this.key = Optional.ofNullable(key);
		this.value = Optional.ofNullable(value);
	}

	public Optional<K> getKeyOfNullable() {
		return key;
	}

	public Optional<V> getValueOfNullable() {
		return value;
	}

	public void setKeyOfNullable(K key) {
		this.key = Optional.ofNullable(key);
	}

	public void setValueOfNullable(V value) {
		this.value = Optional.ofNullable(value);
	}

	public K getKey() {
		return key.get();
	}

	public V getValue() {
		return value.get();
	}

	public void setKey(K key){
		this.key = Optional.of(key);
	}

	public void setValue(V value){
		this.value = Optional.of(value);
	}

	@Override
	public String toString() {
		return "SimpleEntry [key=" + key + ", value=" + value + "]";
	}
	
}
