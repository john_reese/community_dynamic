package cn.jhz.learn.community_dynamic.common.util;

import java.util.function.Consumer;

import cn.jhz.learn.community_dynamic.common.exception.RequestException;
import cn.jhz.learn.community_dynamic.vo.wbms.ColumnType;

public abstract class VerifyHelper {
    
    public static final Consumer<ColumnType> defaultAction = (columnType)-> {
	throw new RequestException("未知字段:" + columnType.name() + '!');
    };
    
    public static <T> void verify(T view, Consumer<ColumnType> detail) {
	boolean failed = false;
	StringBuilder errorMsg = new StringBuilder();

	for (Object columnType : RequestHelper.getPatchColumns()) {
	    try {
		detail.accept((ColumnType) columnType);
	    } catch (RequestException e) {
		failed = true;
		errorMsg.append(e.getMessage());
	    }
	}

	if (failed) {
	    throw new RequestException(errorMsg.toString());
	}
    }
    
    public static <T,U> void createVerify(T view, U[] values, Consumer<U> detail) {
	boolean failed = false;
	StringBuilder errorMsg = new StringBuilder();

	for (U type : values) {
	    try {
		detail.accept(type);
	    } catch (RequestException e) {
		failed = true;
		errorMsg.append(e.getMessage());
	    }
	}

	if (failed) {
	    throw new RequestException(errorMsg.toString());
	}
    }
}
