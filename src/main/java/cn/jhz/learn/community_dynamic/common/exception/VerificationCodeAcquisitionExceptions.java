package cn.jhz.learn.community_dynamic.common.exception;

public class VerificationCodeAcquisitionExceptions extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 6670696786857108909L;

    public VerificationCodeAcquisitionExceptions() {
    }

    public VerificationCodeAcquisitionExceptions(String message) {
        super(message);
    }

    public VerificationCodeAcquisitionExceptions(String message, Throwable cause) {
        super(message, cause);
    }

    public VerificationCodeAcquisitionExceptions(Throwable cause) {
        super(cause);
    }

    public VerificationCodeAcquisitionExceptions(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
