package cn.jhz.learn.community_dynamic.common.exception;

public class SMSException extends RuntimeException{
    /**
     * 
     */
    private static final long serialVersionUID = -3821166707240243247L;

    public SMSException() {
    }

    public SMSException(String message) {
        super(message);
    }

    public SMSException(String message, Throwable cause) {
        super(message, cause);
    }

    public SMSException(Throwable cause) {
        super(cause);
    }

    public SMSException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
