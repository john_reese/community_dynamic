/**
 * 
 */
package cn.jhz.learn.community_dynamic.common.exception;

/**
 * @author machine005
 *
 */
public class RequestException extends RuntimeException{

    /**
     * 
     */
    private static final long serialVersionUID = -1116290551392878672L;
    public RequestException() {
    }

    public RequestException(String message) {
        super(message);
    }

    public RequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestException(Throwable cause) {
        super(cause);
    }
    public RequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
