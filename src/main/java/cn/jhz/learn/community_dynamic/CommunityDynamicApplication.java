package cn.jhz.learn.community_dynamic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

import cn.jhz.learn.community_dynamic.config.Init;
import cn.jhz.learn.community_dynamic.service.CommentService;
import cn.jhz.learn.community_dynamic.service.ImageService;
import cn.jhz.learn.community_dynamic.service.PostService;

@EnableCaching
@SpringBootApplication
public class CommunityDynamicApplication {
    
    static public final String REVIEW_CACHE = "REVIEW_CACHE";
    
    @Bean
    public PasswordEncoder passwordEncoder() {
	return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }
	    
    @Bean
    public Init init(PostService postService, CommentService commentService, ImageService imageService) {
	return new Init(postService, commentService, imageService);
    }
    
    public static void main(String[] args) {
	SpringApplication.run(CommunityDynamicApplication.class, args);
    }

}
