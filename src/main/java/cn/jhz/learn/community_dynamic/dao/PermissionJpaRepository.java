package cn.jhz.learn.community_dynamic.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import cn.jhz.learn.community_dynamic.model.AclEntity;

/**
 * @author machine005
 *
 */
public interface PermissionJpaRepository extends JpaRepository<AclEntity, Integer> {
   
    int countByIdIn(Iterable<Integer> ids);
    boolean existsByname(String name);
    List<AclEntity> findByType(String type);
    @Modifying
    @Query("update AclEntity ae set ae.status = ?1 where ae.id = ?2")
    int setFixedStatusFor(Byte status, Integer id);
}
