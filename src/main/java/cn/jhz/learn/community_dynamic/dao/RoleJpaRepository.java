/**
 * 
 */
package cn.jhz.learn.community_dynamic.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.jhz.learn.community_dynamic.model.RoleEntity;

/**
 * @author machine005
 *
 */
public interface RoleJpaRepository extends JpaRepository<RoleEntity, Integer> {
    Optional<RoleEntity> findByName(String name);
    boolean existsByname(String name);
    int countByIdIn(Iterable<Integer> ids);
}
