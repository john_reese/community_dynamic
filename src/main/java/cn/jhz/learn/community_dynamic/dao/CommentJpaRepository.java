package cn.jhz.learn.community_dynamic.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import cn.jhz.learn.community_dynamic.model.CommentEntity;
import cn.jhz.learn.community_dynamic.model.PostEntity;
import cn.jhz.learn.community_dynamic.model.UserEntity;

public interface CommentJpaRepository extends JpaRepository<CommentEntity, Integer>{
    Optional<CommentEntity> findByIdAndStatus(Integer id, Byte Status);
    List<CommentEntity> findByPostAndStatus(PostEntity entity, Byte status);
    Page<CommentEntity> findByPost(PostEntity entity, Pageable page);
    Page<CommentEntity> findByUser(UserEntity entity, Pageable page);
    Integer countByPostAndStatus(PostEntity entity, Byte status);
    Integer countByUserAndStatus(UserEntity entity, Byte status);
}
