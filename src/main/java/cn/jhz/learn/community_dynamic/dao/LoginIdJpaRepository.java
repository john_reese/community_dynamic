package cn.jhz.learn.community_dynamic.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.jhz.learn.community_dynamic.model.LoginIdEntity;

public interface LoginIdJpaRepository extends JpaRepository<LoginIdEntity, Long> {
    LoginIdEntity findFirstByStatus(Byte status);
}
