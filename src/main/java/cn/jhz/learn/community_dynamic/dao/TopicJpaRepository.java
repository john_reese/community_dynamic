package cn.jhz.learn.community_dynamic.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import cn.jhz.learn.community_dynamic.model.TopicEntity;

public interface TopicJpaRepository extends JpaRepository<TopicEntity, Integer> {
    List<TopicEntity> findByTopicClass_id(Integer topicClassId, Pageable pageable);
    List<TopicEntity> findByType(Byte type, Pageable pageable);
    Page<TopicEntity> findByTitleContaining(String title, Pageable pageable);
}
