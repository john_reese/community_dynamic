package cn.jhz.learn.community_dynamic.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.jhz.learn.community_dynamic.model.AdsenseEntity;

public interface AdsenseJpaRepository extends JpaRepository<AdsenseEntity, Integer>{
    List<AdsenseEntity> findByType(Byte type);
}
