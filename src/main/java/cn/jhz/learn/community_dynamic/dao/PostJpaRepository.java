package cn.jhz.learn.community_dynamic.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import cn.jhz.learn.community_dynamic.model.PostClassEntity;
import cn.jhz.learn.community_dynamic.model.PostEntity;
import cn.jhz.learn.community_dynamic.model.TopicClassEntity;
import cn.jhz.learn.community_dynamic.model.TopicEntity;
import cn.jhz.learn.community_dynamic.model.UserEntity;

public interface PostJpaRepository extends JpaRepository<PostEntity, Integer> {
    Optional<PostEntity> findByIdAndStatus(Integer id, Byte status);
    Page<PostEntity> findByPostClassAndStatusInAndIsOpenIn(PostClassEntity classEntity, Collection<Byte> status, Collection<Byte> isOpen, Pageable pageable);
    Page<PostEntity> findByTopicsAndStatusInAndIsOpenIn(TopicEntity topicEntity, Collection<Byte> status, Collection<Byte> isOpen, Pageable pageable);
    Page<PostEntity> findByTitleContainingAndStatusInAndIsOpenIn(String title, Collection<Byte> status, Collection<Byte> isOpen, Pageable pageable);
    List<PostEntity> findByUser_loginIdAndStatusAndIsOpenIn(Long loginId,Byte status, Collection<Byte> isOpen, Pageable pageable);
    Page<PostEntity> findByStatusAndIsOpenAndTitleContainingOrContentContaining(Byte status, Byte isOpen, String titleKeyword, String contentKeyword,Pageable pageable);
    Page<PostEntity> findByUser_followedsAndStatusAndIsOpen(UserEntity userEntity, Byte status, Byte isOpen, Pageable pageable);
    Page<PostEntity> findByUser(UserEntity entity, Pageable pageable);
    Integer countByTopics_idAndStatusAndCreateTimeAfter(Integer topicId, Byte status, Date time);
    Integer countByTopics_idAndStatus(Integer topicId, Byte status);
    Integer countByUserAndStatusAndCreateTimeAfter(UserEntity entity, Byte status, Date time);
    Integer countByUserAndStatus(UserEntity entity, Byte status);
    Integer countSupportUsersByIdAndStatus(Integer id, Byte status);
    Integer countUnSupportUsersByIdAndStatus(Integer id, Byte status);
    Integer countSupportUsersByUserAndStatus(UserEntity userEntity, Byte status);
    Integer countUnSupportUsersByUserAndStatus(UserEntity userEntity, Byte status);
    
    Long countByTopics_topicClassAndStatusAndCreateTimeBetween(TopicClassEntity entity, Byte status, Date timeAfter, Date timeBefore);
    Long countByTopics_topicClassAndStatus(TopicClassEntity entity, Byte status);
    Optional<PostEntity> findTopByOrderByCreateTimeAsc();
//    @Modifying
//    @Query("update PostEntity pe set pe.status = ?1 where pe = ?2")
//    Integer setFixedUserpicFor(Byte status, PostEntity entity);
//    @Query(value="select count(*) from PostEntity pe join pe.topics te where te.id = ?1 and pe.status = ?2 and DateDiff(pe.createTime,NOW())=0")
//    Integer countTodayByTopicId(Integer topicId, Byte status);
//    @Query(value="select pe from PostEntity pe join pe.topics te where te.id = ?1 and pe.status = ?2")
//    List<PostEntity> findAllByTopic_id(Integer topicId, Byte status, Pageable pageable);
}