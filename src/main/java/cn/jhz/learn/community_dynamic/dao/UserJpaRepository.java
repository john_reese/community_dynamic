package cn.jhz.learn.community_dynamic.dao;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import cn.jhz.learn.community_dynamic.model.UserEntity;
import cn.jhz.learn.community_dynamic.model.UserInfoEntity;

/**
 * @author machine005
 *
 */
public interface UserJpaRepository extends JpaRepository<UserEntity, Integer> {
    Optional<UserEntity> findByPhone(String phone);
    Optional<UserEntity> findByEmail(String email);
    Optional<UserEntity> findByLoginId(Long loginId);
    Optional<UserEntity> findByUsername(String username);
    Page<UserEntity> findByStatusAndUsernameContaining(Byte status, String username, Pageable page);
    Page<UserEntity> findByFollowsAndFollowedsAndIdNot(UserEntity follow, UserEntity followed, Integer id, Pageable page);
    Page<UserEntity> findByFollows(UserEntity follow,  Pageable page);
    Page<UserEntity> findByFolloweds(UserEntity follow,  Pageable page);
//    Optional<UserEntity> findByBlack_idAndBlacked_id(Integer id, Integer blackId);
//    Optional<UserEntity> findByFollows_IdAndFolloweds_id(Integer id, Integer followId);
    Integer countByRoles_id(Integer roleId);
    Integer countByFolloweds(UserEntity entity);
    Integer countByFollows(UserEntity entity);
    Integer countByFollowsAndFollowedsAndIdNot(UserEntity f_entity, UserEntity fed_entity, Integer id);
    boolean existsByusername(String username);
    boolean existsByemail(String email);
    boolean existsByphone(String phone);
    boolean existsByloginId(Long loginId);
//    boolean existsById(Integer id);
    
    @Modifying
    @Query("update UserEntity ue set ue.phone = ?1 where ue = ?2")
    Integer setFixedPhoneFor(String phone, UserEntity entity);
    
    @Modifying
    @Query("update UserEntity ue set ue.email = ?1 where ue = ?2")
    Integer setFixedEmailFor(String email, UserEntity entity);
    
    @Modifying
    @Query("update UserEntity ue set ue.userInfo = ?1 where ue = ?2")
    Integer setFixedUserInfoFor(UserInfoEntity userInfoEntity, UserEntity userEntity);
    
    @Modifying
    @Query("update UserEntity ue set ue.password = ?1 where ue = ?2")
    Integer setFixedPasswordFor(String password, UserEntity userEntity);
    
    @Modifying
    @Query("update UserEntity ue set ue.username = ?1 where ue = ?2")
    Integer setFixedUsernameFor(String username, UserEntity userEntity);
    
    @Modifying
    @Query("update UserEntity ue set ue.userpic = ?1 where ue = ?2")
    Integer setFixedUserpicFor(String userpic, UserEntity userEntity);
//    @Modifying
//    @Query("insert into UserEntity(black) select i.itemId,i.itemName,i.itemPrice from Items i where i.itemId= ?")
//    Integer addFixedBlackFor(UserEntity black, UserEntity userEntity);
}
