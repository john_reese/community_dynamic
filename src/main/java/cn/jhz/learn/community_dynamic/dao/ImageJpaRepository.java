package cn.jhz.learn.community_dynamic.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.jhz.learn.community_dynamic.model.ImageEntity;

public interface ImageJpaRepository extends JpaRepository<ImageEntity, Integer> {
    int countByUser_idAndIdIn(Integer userId,Iterable<Integer> ids);
}
