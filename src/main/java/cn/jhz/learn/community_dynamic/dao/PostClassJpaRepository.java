package cn.jhz.learn.community_dynamic.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.jhz.learn.community_dynamic.model.PostClassEntity;

public interface PostClassJpaRepository extends JpaRepository<PostClassEntity, Integer> {
    List<PostClassEntity> findAllByStatus(Byte status);
}
