package cn.jhz.learn.community_dynamic.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import cn.jhz.learn.community_dynamic.model.TopicClassEntity;

public interface TopicClassJpaRepository extends JpaRepository<TopicClassEntity, Integer> {
    List<TopicClassEntity> findAllByStatus(Byte status);
}
