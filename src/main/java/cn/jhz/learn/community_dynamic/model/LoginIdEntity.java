package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The persistent class for the id_pool database table.
 * 
 */
@Entity
@Table(name = "id_pool")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class LoginIdEntity implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = -2671577810344784431L;
    private Long id;
    private Byte status;

    public LoginIdEntity() {
	this.status = 0;
    }
    
    public LoginIdEntity(Long id) {
	this.id = id;
	this.status = 0;
    }
    @Id
    @Column(unique = true)
    public Long getId() {
	return this.id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    @Column(nullable = true)
    public Byte getStatus() {
	return this.status;
    }

    public void setStatus(Byte status) {
	this.status = status;
    }

}