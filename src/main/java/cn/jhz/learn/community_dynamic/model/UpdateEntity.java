package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


/**
 * The persistent class for the update database table.
 * 
 */
@Entity
@Table(name = "update")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class UpdateEntity implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = 7629560362383042589L;
    private Integer id;
    private Date createTime;
    private Byte status;
    private String url;
    private Float version;

    public UpdateEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Column(name = "create_time", nullable = false)
    public Date getCreateTime() {
	return this.createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    @Column(nullable = false)
    public Byte getStatus() {
	return this.status;
    }

    public void setStatus(Byte status) {
	this.status = status;
    }

    @Column(nullable = false, length = 500)
    public String getUrl() {
	return this.url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    @Column(nullable = false)
    public Float getVersion() {
	return this.version;
    }

    public void setVersion(Float version) {
	this.version = version;
    }

}