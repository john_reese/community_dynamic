package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the topic_class database table.
 * 
 */
@Entity
@Table(name = "topic_class")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TopicClassEntity implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = 7835288111110062051L;
    private Integer id;
    private String className;
    private Date createTime;
    private Byte status;
    private Set<TopicEntity> topics;

    public TopicClassEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Column(name = "class_name", nullable = false, length = 50)
    public String getClassName() {
	return this.className;
    }

    public void setClassName(String className) {
	this.className = className;
    }

    @Column(name = "create_time", nullable = false)
    public Date getCreateTime() {
	return this.createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    @Column(nullable = false)
    public Byte getStatus() {
	return this.status;
    }

    public void setStatus(Byte status) {
	this.status = status;
    }

    // bi-directional many-to-one association to Topic
    @OneToMany(mappedBy = "topicClass")
    public Set<TopicEntity> getTopics() {
	return this.topics;
    }

    public void setTopics(Set<TopicEntity> topics) {
	this.topics = topics;
    }

    public TopicEntity addTopic(TopicEntity topic) {
	getTopics().add(topic);
	topic.setTopicClass(this);

	return topic;
    }

    public TopicEntity removeTopic(TopicEntity topic) {
	getTopics().remove(topic);
	topic.setTopicClass(null);

	return topic;
    }

}