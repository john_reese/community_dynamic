package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Date;

/**
 * The persistent class for the account_userinfo database table.
 * 
 */
@Entity
@Table(name = "user_info", schema = "community_dynamic")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@JsonIgnoreProperties(value = {"hibernateLazyInitializer"})
public class UserInfoEntity implements Serializable {
    private static final long serialVersionUID = -7188672725760584704L;
    @JsonIgnore
    private Integer id;
    private String address;
    private byte age;
    private Date birthday;
    private String job;
    private byte qg;
    private byte sex;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }
    
    public void setId(Integer id) {
	this.id = id;
    }

    @Column(length = 128)
    public String getAddress() {
	return this.address;
    }

    public void setAddress(String address) {
	this.address = address;
    }

    public byte getAge() {
	return this.age;
    }

    public void setAge(byte age) {
	this.age = age;
    }

    public Date getBirthday() {
	return this.birthday;
    }

    public void setBirthday(Date birthday) {
	this.birthday = birthday;
    }

    @Column(length = 45)
    public String getJob() {
	return this.job;
    }

    public void setJob(String job) {
	this.job = job;
    }

    public byte getQg() {
	return this.qg;
    }

    public void setQg(byte qg) {
	this.qg = qg;
    }

    public byte getSex() {
	return this.sex;
    }

    public void setSex(byte sex) {
	this.sex = sex;
    }
}