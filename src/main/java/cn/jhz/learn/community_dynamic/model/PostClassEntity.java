package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the post_class database table.
 * 
 */
@Entity
@Table(name = "post_class")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class PostClassEntity implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = -5282437696966598488L;
    private Integer id;
    private String className;
    private Date createTime;
    private Byte status;
    private Set<PostEntity> posts;

    public PostClassEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Column(name = "class_name", nullable = false, length = 50)
    public String getClassName() {
	return this.className;
    }

    public void setClassName(String className) {
	this.className = className;
    }

    @Column(name = "create_time", nullable = false)
    public Date getCreateTime() {
	return this.createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    @Column(nullable = false)
    public Byte getStatus() {
	return this.status;
    }

    public void setStatus(Byte status) {
	this.status = status;
    }

    // bi-directional many-to-many association to Post
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
    @OneToMany(mappedBy = "postClass", fetch = FetchType.LAZY)
    public Set<PostEntity> getPosts() {
	return this.posts;
    }

    public void setPosts(Set<PostEntity> posts) {
	this.posts = posts;
    }

}