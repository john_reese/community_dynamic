package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 * The persistent class for the account_user_bind database table.
 * 
 */
@Entity
@Table(name = "user_bind", schema = "community_dynamic")
@NamedQuery(name = "UserBindEntity.findAll", query = "SELECT a FROM UserBindEntity a")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserBindEntity implements Serializable {
    private static final long serialVersionUID = -5398490551589727730L;
    private Integer id;
    private String avatarurl;
    private String nickname;
    private String openId;
    private String type;
    private UserEntity user;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Column(nullable = false, length = 128)
    public String getAvatarurl() {
	return this.avatarurl;
    }

    public void setAvatarurl(String avatarurl) {
	this.avatarurl = avatarurl;
    }

    @Column(nullable = false, length = 45)
    public String getNickname() {
	return this.nickname;
    }

    public void setNickname(String nickname) {
	this.nickname = nickname;
    }

    @Column(name = "open_id", nullable = false, length = 256)
    public String getOpenId() {
	return this.openId;
    }

    public void setOpenId(String openId) {
	this.openId = openId;
    }

    @Column(nullable = false, length = 12)
    public String getType() {
	return this.type;
    }

    public void setType(String type) {
	this.type = type;
    }

    // bi-directional many-to-one association to AccountUser
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    public UserEntity getUser() {
	return this.user;
    }

    public void setUser(UserEntity user) {
	this.user = user;
    }

}