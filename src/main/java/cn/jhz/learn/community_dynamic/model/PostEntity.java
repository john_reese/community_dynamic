package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the post database table.
 * 
 */
@Entity
@Table(name = "post")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AllArgsConstructor
@Builder
public class PostEntity implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = -2717601696797668247L;
    private Integer id;
    private String content;
    private Date createTime;
    private Byte isOpen;
    private String path;
    private Integer shareCount;
    private PostEntity share;
    private Set<PostEntity> shared;
    private String title;
    private String titlepic;
    private Byte type;
    private Byte status;
    private UserEntity user;
    private Set<CommentEntity> comments;
    private Set<UserEntity> supportUsers;
    private Set<UserEntity> unSupportUsers;
    private PostClassEntity postClass;
    private Set<ImageEntity> images;
    private Set<TopicEntity> topics;

    public PostEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Column(nullable = false, length = 5000)
    public String getContent() {
	return this.content;
    }

    public void setContent(String content) {
	this.content = content;
    }

    @Column(name = "create_time", nullable = false)
    public Date getCreateTime() {
	return this.createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    @Column(name = "is_open", nullable = false)
    public Byte getIsOpen() {
	return this.isOpen;
    }

    public void setIsOpen(Byte isOpen) {
	this.isOpen = isOpen;
    }

    @Column(nullable = false, length = 50)
    public String getPath() {
	return this.path;
    }

    public void setPath(String path) {
	this.path = path;
    }

    @Column(name = "share_count", nullable = false)
    public Integer getShareCount() {
	return this.shareCount;
    }

    public void setShareCount(Integer shareCount) {
	this.shareCount = shareCount;
    }

    @Column(nullable = false, length = 50)
    public String getTitle() {
	return this.title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    @Column(nullable = false, length = 50)
    public String getTitlepic() {
	return this.titlepic;
    }

    public void setTitlepic(String titlepic) {
	this.titlepic = titlepic;
    }

    @Column(nullable = false)
    public Byte getType() {
	return this.type;
    }

    public void setType(Byte type) {
	this.type = type;
    }
    
    @Column(nullable = false)
    public Byte getStatus() {
	return this.status;
    }

    public void setStatus(Byte status) {
	this.status = status;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    public UserEntity getUser() {
	return this.user;
    }

    public void setUser(UserEntity user) {
	this.user = user;
    }

    // bi-directional many-to-one association to Comment
    @OneToMany(mappedBy = "post")
    public Set<CommentEntity> getComments() {
	return this.comments;
    }

    public void setComments(Set<CommentEntity> comments) {
	this.comments = comments;
    }

    public CommentEntity addComment(CommentEntity comment) {
	getComments().add(comment);
	comment.setPost(this);

	return comment;
    }

    public CommentEntity removeComment(CommentEntity comment) {
	getComments().remove(comment);
	comment.setPost(null);

	return comment;
    }

    // bi-directional many-to-many association to PostClass
    @ManyToOne
    @JoinColumn(name = "post_class_id", nullable = false)
    public PostClassEntity getPostClass() {
	return this.postClass;
    }

    public void setPostClass(PostClassEntity postClass) {
	this.postClass = postClass;
    }

    // bi-directional many-to-many association to Image
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
    @ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinTable(name = "post_image", schema = "community_dynamic", joinColumns = {
	    @JoinColumn(name = "post_id", nullable = false) }, inverseJoinColumns = {
		    @JoinColumn(name = "image_id", nullable = false) })
    public Set<ImageEntity> getImages() {
	return this.images;
    }

    public void setImages(Set<ImageEntity> images) {
	this.images = images;
    }

    // bi-directional many-to-many association to Topic
    @ManyToMany(mappedBy = "posts")
    public Set<TopicEntity> getTopics() {
	return this.topics;
    }

    public void setTopics(Set<TopicEntity> topics) {
	this.topics = topics;
    }

    @OneToMany(mappedBy = "share")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
    public Set<PostEntity> getShared() {
        return shared;
    }

    public void setShared(Set<PostEntity> shared) {
        this.shared = shared;
    }
    
    @ManyToOne
    @JoinColumn(name = "share_id")
    public PostEntity getShare() {
	return this.share;
    }

    public void setShare(PostEntity share) {
	this.share = share;
    }
    
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
    @ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinTable(name = "support", schema = "community_dynamic", joinColumns = {
	    @JoinColumn(name = "post_id", nullable = false) }, inverseJoinColumns = {
		    @JoinColumn(name = "user_id", nullable = false) })
    public Set<UserEntity> getSupportUsers() {
	return this.supportUsers;
    }

    public void setSupportUsers(Set<UserEntity> supportUsers) {
	this.supportUsers = supportUsers;
    }
    
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
    @ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinTable(name = "unsupport", schema = "community_dynamic", joinColumns = {
	    @JoinColumn(name = "post_id", nullable = false) }, inverseJoinColumns = {
		    @JoinColumn(name = "user_id", nullable = false) })
    public Set<UserEntity> getUnSupportUsers() {
	return this.unSupportUsers;
    }

    public void setUnSupportUsers(Set<UserEntity> unsupportUsers) {
	this.unSupportUsers = unsupportUsers;
    }
}