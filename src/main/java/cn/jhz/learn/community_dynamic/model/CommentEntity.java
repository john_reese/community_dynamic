package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the comment database table.
 * 
 */
@Entity
@Table(name = "comment")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@AllArgsConstructor
@Builder
public class CommentEntity implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = -5851892204875520352L;
    private Integer id;
    private Date createTime;
    private String data;
    private CommentEntity fatherComment;
    private Set<CommentEntity> childrenComments;
    private Integer fnum;
    private UserEntity user;
    private PostEntity post;
    private Byte status;

    public CommentEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Column(name = "create_time", nullable = false)
    public Date getCreateTime() {
	return this.createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    @Column(nullable = false, length = 140)
    public String getData() {
	return this.data;
    }

    public void setData(String data) {
	this.data = data;
    }

    @Column(nullable = false)
    public Integer getFnum() {
	return this.fnum;
    }

    public void setFnum(Integer fnum) {
	this.fnum = fnum;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    public UserEntity getUser() {
	return this.user;
    }

    public void setUser(UserEntity user) {
	this.user = user;
    }

    // bi-directional many-to-one association to Post
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "post_id", nullable = false)
    public PostEntity getPost() {
	return this.post;
    }

    public void setPost(PostEntity post) {
	this.post = post;
    }

    @Column
    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    @ManyToOne
    @JoinColumn(name = "fid")
    public CommentEntity getFatherComment() {
        return fatherComment;
    }

    public void setFatherComment(CommentEntity fatherComment) {
        this.fatherComment = fatherComment;
    }

    @OneToMany(mappedBy = "fatherComment")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
    public Set<CommentEntity> getChildrenComments() {
        return childrenComments;
    }

    public void setChildrenComments(Set<CommentEntity> childrenComments) {
        this.childrenComments = childrenComments;
    }
}