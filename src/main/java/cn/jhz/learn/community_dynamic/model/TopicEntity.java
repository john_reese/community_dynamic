package cn.jhz.learn.community_dynamic.model;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.Date;
import java.util.Set;
import java.io.Serializable;

/**
 * The persistent class for the topic database table.
 * 
 */
@Entity
@Table(name = "topic")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TopicEntity implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = -581206478469384144L;
    private Integer id;
    private Date createTime;
    private String desc;
    private String title;
    private String titlepic;
    private Byte type;
    private TopicClassEntity topicClass;
    private Set<PostEntity> posts;

    public TopicEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Column(name = "create_time", nullable = false)
    public Date getCreateTime() {
	return this.createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    @Column(nullable = false, length = 200)
    public String getDesc() {
	return this.desc;
    }

    public void setDesc(String desc) {
	this.desc = desc;
    }

    @Column(nullable = false, length = 50)
    public String getTitle() {
	return this.title;
    }

    public void setTitle(String title) {
	this.title = title;
    }

    @Column(nullable = false)
    public String getTitlepic() {
	return this.titlepic;
    }

    public void setTitlepic(String titlepic) {
	this.titlepic = titlepic;
    }

    @Column(nullable = false)
    public Byte getType() {
	return this.type;
    }

    public void setType(Byte type) {
	this.type = type;
    }

    // bi-directional many-to-one association to TopicClass
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "topic_class_id", nullable = false)
    public TopicClassEntity getTopicClass() {
	return this.topicClass;
    }

    public void setTopicClass(TopicClassEntity topicClass) {
	this.topicClass = topicClass;
    }

    // bi-directional many-to-many association to Post
    @ManyToMany
    @JoinTable(name = "topic_post", schema = "community_dynamic", joinColumns = {
	    @JoinColumn(name = "topic_id", nullable = false) }, inverseJoinColumns = {
		    @JoinColumn(name = "post_id", nullable = false) })
    public Set<PostEntity> getPosts() {
	return this.posts;
    }

    public void setPosts(Set<PostEntity> posts) {
	this.posts = posts;
    }

}