package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * The persistent class for the account_role database table.
 * 
 */
@Entity
@Table(name = "role", schema = "community_dynamic")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RoleEntity implements Serializable {
    private static final long serialVersionUID = 4001534723216810746L;
    private Integer id;
    private String name;
    private Byte status;
    private Set<AclEntity> acls;
    private Set<UserEntity> users;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Basic
    @Column(nullable = false, length = 50)
    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    // bi-directional many-to-many association to AccountPermission
//    @Fetch(value = FetchMode.SUBSELECT)
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE) 
    @ManyToMany(cascade = { CascadeType.REFRESH, CascadeType.REMOVE, CascadeType.MERGE }, fetch = FetchType.LAZY)
    @JoinTable(name = "role_acl", schema = "community_dynamic", joinColumns = {
	    @JoinColumn(name = "role_id", nullable = false) }, inverseJoinColumns = {
		    @JoinColumn(name = "acl_id", nullable = false) })
    public Set<AclEntity> getAcls() {
	return this.acls;
    }

    public void setAcls(Set<AclEntity> acls) {
	this.acls = acls;
    }

    // bi-directional many-to-many association to AccountUser
    @ManyToMany(mappedBy = "roles")
    public Set<UserEntity> getUsers() {
	return this.users;
    }

    public void setUsers(Set<UserEntity> users) {
	this.users = users;
    }

    @Basic
    @Column(name = "status", nullable = false, length = 50)
    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
}