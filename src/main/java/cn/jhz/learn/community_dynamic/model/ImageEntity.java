package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.Date;
import java.util.Set;

/**
 * The persistent class for the image database table.
 * 
 */
@Entity
@Table(name = "image")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Builder
@AllArgsConstructor
public class ImageEntity implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = -7908127969249766068L;
    private Integer id;
    private Date createTime;
    private String url;
    @JsonIgnoreProperties
    private UserEntity user;
    @JsonIgnoreProperties
    private Set<PostEntity> posts;
    private Byte status;

    public ImageEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Column(name = "create_time", nullable = false)
    public Date getCreateTime() {
	return this.createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    @Column(nullable = false, length = 1000)
    public String getUrl() {
	return this.url;
    }

    public void setUrl(String url) {
	this.url = url;
    }

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    public UserEntity getUser() {
	return this.user;
    }

    public void setUser(UserEntity user) {
	this.user = user;
    }

    // bi-directional many-to-many association to Post
    @ManyToMany(mappedBy = "images")
    public Set<PostEntity> getPosts() {
	return this.posts;
    }

    public void setPosts(Set<PostEntity> posts) {
	this.posts = posts;
    }

    @Column(name = "status", nullable = false)
    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }
    
    

}