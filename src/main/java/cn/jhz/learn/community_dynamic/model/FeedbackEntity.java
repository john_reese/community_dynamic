package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * The persistent class for the feedback database table.
 * 
 */
@Entity
@Table(name = "feedback")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class FeedbackEntity implements Serializable {

    /**
    * 
    */
    private static final long serialVersionUID = -4240027544812369225L;
    private Integer id;
    private Date createTime;
    private String data;
    private UserEntity from;
    private Integer toId;

    public FeedbackEntity() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Column(name = "create_time", nullable = false)
    public Date getCreateTime() {
	return this.createTime;
    }

    public void setCreateTime(Date createTime) {
	this.createTime = createTime;
    }

    @Column(nullable = false, length = 1000)
    public String getData() {
	return this.data;
    }

    public void setData(String data) {
	this.data = data;
    }

    @JoinColumn(name = "user_id", nullable = false)
    @ManyToOne
    public UserEntity getFrom() {
	return this.from;
    }

    public void setFrom(UserEntity from) {
	this.from = from;
    }

    @Column(name = "to_id", nullable = false)
    public Integer getToId() {
	return this.toId;
    }

    public void setToId(Integer toId) {
	this.toId = toId;
    }

}