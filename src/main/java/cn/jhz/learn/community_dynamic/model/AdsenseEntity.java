package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import java.util.Date;


/**
 * The persistent class for the adsense database table.
 * 
 */
@Entity
@Table(name="adsense")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class AdsenseEntity implements Serializable {
    
	/**
     * 
     */
    private static final long serialVersionUID = -181678542957323100L;
	private Integer id;
	private Date createTime;
	private String src;
	private Byte type;
	private String url;

	public AdsenseEntity() {
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	@Column(name="create_time", nullable=false)
	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}


	@Column(nullable=false, length=1000)
	public String getSrc() {
		return this.src;
	}

	public void setSrc(String src) {
		this.src = src;
	}


	@Column(nullable=false)
	public Byte getType() {
		return this.type;
	}

	public void setType(Byte type) {
		this.type = type;
	}


	@Column(nullable=false, length=1000)
	public String getUrl() {
		return this.url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}