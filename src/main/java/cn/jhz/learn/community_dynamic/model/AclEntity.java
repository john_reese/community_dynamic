package cn.jhz.learn.community_dynamic.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;


/**
 * The persistent class for the account_permission database table.
 * 
 */
@Entity
@Table(name = "acl", schema = "community_dynamic")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AclEntity implements Serializable {
    private static final long serialVersionUID = 7570525820411501292L;
    private Integer id;
    private String name;
    private String type;
    private String resource;
    private Integer parentId;
    private Byte seq;
    private Byte status;
    private Set<RoleEntity> roles;
     
    public AclEntity(String name, String resource, String type, Integer parentId, Byte status) {
	this.name = name;
	this.resource = resource;
	this.type = type;
	this.parentId = parentId;
	this.status = status;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    public Integer getId() {
	return this.id;
    }

    public void setId(Integer id) {
	this.id = id;
    }

    @Basic
    @Column(nullable = false, length = 50)
    public String getName() {
	return this.name;
    }

    public void setName(String name) {
	this.name = name;
    }

    @Basic
    @Column(nullable = false, length = 255)
    public String getResource() {
	return this.resource;
    }

    public void setResource(String resource) {
	this.resource = resource;
    }
    
    @Basic
    @Column(nullable = false, length = 255)
    public String getType() {
	return this.type;
    }

    public void setType(String type) {
	this.type = type;
    }
    

    // bi-directional many-to-many association to AccountRole
    @ManyToMany(mappedBy = "acls")
    public Set<RoleEntity> getRoles() {
	return this.roles;
    }

    public void setRoles(Set<RoleEntity> roles) {
	this.roles = roles;
    }

    @Basic
    @Column(name = "parent_id", nullable = false)
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    @Basic
    @Column(name = "seq", nullable = false)
    public Byte getSeq() {
        return seq;
    }

    public void setSeq(Byte seq) {
        this.seq = seq;
    }
}