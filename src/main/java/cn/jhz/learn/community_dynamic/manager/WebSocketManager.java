package cn.jhz.learn.community_dynamic.manager;

import java.util.Optional;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

@Service
public class WebSocketManager {
    
    @Cacheable(cacheNames = "webSocketSessionCache", key = "#loginId", unless = "#result == null")
    public Optional<WebSocketSession> cacheInstance(String loginId, WebSocketSession session){
        return Optional.ofNullable(session);
    }
    
    @CacheEvict(cacheNames="webSocketSessionCache", allEntries=true)
    public void clearInstance(String loginId){}
    
    @CachePut(cacheNames = "webSocketSessionCache", unless = "#result == null")
    public Optional<WebSocketSession> putInstance(String loginId, WebSocketSession session){
        return Optional.empty();
    }

    @Cacheable(cacheNames = "webSocketSessionCache", unless = "#result == null")
    public Optional<WebSocketSession> getInstance(String loginId){
        return Optional.empty();
    }
}
