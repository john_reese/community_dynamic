package cn.jhz.learn.community_dynamic.manager;

import java.util.Optional;
import java.util.Queue;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import cn.jhz.learn.community_dynamic.vo.app.MsgView;

@Service
public class MsgManager {
    
    @Cacheable(cacheNames = "msgQueueCache", key = "#longId", unless = "#result == null")
    public Optional<Queue<MsgView>> cacheInstance(String longId,  Queue<MsgView> queue){
        return Optional.ofNullable(queue);
    }
    

    @CacheEvict(cacheNames="msgQueueCache", allEntries=true)
    public void clearInstance(String longId){}


    @Cacheable(cacheNames = "msgQueueCache", unless = "#result == null")
    public Optional<Queue<MsgView>> getInstance(String longId){
        return Optional.empty();
    }
    
    @CachePut(cacheNames="msgQueueCache", key = "#longId", unless = "#result == null")
    public Optional<Queue<MsgView>> putInstance(String longId,  Queue<MsgView> queue){
        return Optional.ofNullable(queue);
    }
}
