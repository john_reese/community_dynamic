package cn.jhz.learn.community_dynamic.manager;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

@Service
public class EmailManager {
    private final JavaMailSender mailSender;
    static private final String EMAIL_SENDER = "MACHINE005<john_b_reese@163.com>";

    @Autowired
    public EmailManager(JavaMailSender mailSender) {
	super();
	this.mailSender = mailSender;
    }

    public void sendSimpleEmail(String to, String subject, String content) {
	SimpleMailMessage message = new SimpleMailMessage();
	message.setFrom(EMAIL_SENDER);
	message.setTo(to);
	message.setSubject(subject);
	message.setText(content);
	try {
	    mailSender.send(message);
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    /**
     * .发送 HTML 邮件
     *
     * @param to      收件人
     * @param subject 主题
     * @param content 内容
     */
    public void sendMimeMessge(String to, String subject, String content) {
	MimeMessage message = mailSender.createMimeMessage();
	try {
	    // true表示需要创建一个multipart message
	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    helper.setFrom(EMAIL_SENDER);
	    helper.setTo(to);
	    helper.setSubject(subject);
	    helper.setText(content, true);
	    mailSender.send(message);
	} catch (MessagingException e) {
	    e.printStackTrace();
	}
    }

    /**
     * 发送带附件的邮件
     *
     * @param to       收件人
     * @param subject  主题
     * @param content  内容
     * @param filePath 附件路径
     */
//    @Override
//    public void sendMimeMessge(String to, String subject, String content, String filePath) {
//	MimeMessage message = mailSender.createMimeMessage();
//	try {
//	    // true表示需要创建一个multipart message
//	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
//	    helper.setFrom(SENDER);
//	    helper.setTo(to);
//	    helper.setSubject(subject);
//	    helper.setText(content, true);
//
//	    FileSystemResource file = new FileSystemResource(new File(filePath));
//	    String fileName = file.getFilename();
//	    helper.addAttachment(fileName, file);
//
//	    mailSender.send(message);
//	} catch (MessagingException e) {
//	    e.printStackTrace();
//	}
//    }

    /**
     * 发送带静态文件的邮件
     *
     * @param to       收件人
     * @param subject  主题
     * @param content  内容
     * @param rscIdMap 需要替换的静态文件
     */
//    @Override
//    public void sendMimeMessge(String to, String subject, String content, Map<String, String> rscIdMap) {
//	MimeMessage message = mailSender.createMimeMessage();
//	try {
//	    // true表示需要创建一个multipart message
//	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
//	    helper.setFrom(SENDER);
//	    helper.setTo(to);
//	    helper.setSubject(subject);
//	    helper.setText(content, true);
//
//	    for (Map.Entry<String, String> entry : rscIdMap.entrySet()) {
//		FileSystemResource file = new FileSystemResource(new File(entry.getValue()));
//		helper.addInline(entry.getKey(), file);
//	    }
//	    mailSender.send(message);
//	} catch (MessagingException e) {
//	    logger.error("发送带静态文件的MimeMessge时发生异常！", e);
//	}
//    }
}
