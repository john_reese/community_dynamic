package cn.jhz.learn.community_dynamic.manager;

import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class VerificationCodeManager {

    public String newInstance(String phone){
        return String.valueOf((int)((Math.random() * 9 + 1) * Math.pow(10, 6 - 1)));
    }

    @Cacheable(cacheNames = "verificationCache", key = "#key", unless = "#result == null")
    public Optional<String> cacheInstance(String key, String code){
        return Optional.ofNullable(StringUtils.isNotBlank(code) ? code : null);
    }
    

    @CacheEvict(cacheNames="verificationCache", allEntries=true)
    public void clearInstance(String key){}


    @Cacheable(cacheNames = "verificationCache", unless = "#result == null")
    public Optional<String> getInstance(String key){
        return Optional.empty();
    }

}
