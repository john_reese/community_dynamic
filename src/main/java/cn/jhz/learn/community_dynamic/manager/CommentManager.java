package cn.jhz.learn.community_dynamic.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.jhz.learn.community_dynamic.dao.CommentJpaRepository;
import cn.jhz.learn.community_dynamic.model.CommentEntity;

@Service
public class CommentManager {
    private final CommentJpaRepository commentJpaRepository;
    
    @Autowired
    public CommentManager(CommentJpaRepository commentJpaRepository) {
	super();
	this.commentJpaRepository = commentJpaRepository;
    }

    @Transactional
    public CommentEntity updateInstanceColumn(CommentEntity entity, Byte status) {
	entity.setStatus(status);
	return entity;
    }
    
    @Transactional
    public CommentEntity addInstance(CommentEntity entity) {
	return this.commentJpaRepository.saveAndFlush(entity);
    }
}
