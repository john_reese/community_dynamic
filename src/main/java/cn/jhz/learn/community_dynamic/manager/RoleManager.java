package cn.jhz.learn.community_dynamic.manager;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import cn.jhz.learn.community_dynamic.common.util.RequestHelper;
import cn.jhz.learn.community_dynamic.dao.PermissionJpaRepository;
import cn.jhz.learn.community_dynamic.dao.RoleJpaRepository;
import cn.jhz.learn.community_dynamic.model.AclEntity;
import cn.jhz.learn.community_dynamic.model.RoleEntity;
import cn.jhz.learn.community_dynamic.vo.wbms.ColumnType;
import cn.jhz.learn.community_dynamic.vo.wbms.RoleView;

@Service
public class RoleManager {
    private final RoleJpaRepository roleJpaRepository;
    private final PermissionJpaRepository permissionJpaRepository;

    @Autowired
    public RoleManager(RoleJpaRepository roleJpaRepository, PermissionJpaRepository permissionJpaRepository) {
	super();
	this.roleJpaRepository = roleJpaRepository;
	this.permissionJpaRepository = permissionJpaRepository;
    }
    
    public RoleEntity newInstance(RoleView role) {
	Set<AclEntity> temp = new HashSet<>();
	if(role.getPermissions() != null) {
	    temp.addAll(this.permissionJpaRepository.findAllById(role.getPermissions()));
	}
	return RoleEntity.builder().name(role.getName()).status(role.getStatus()).acls(temp).build();
    }
    
    @Transactional
    public RoleEntity addInstance(RoleEntity entity) {
	return this.roleJpaRepository.save(entity);
    }
    
    @Transactional
    public RoleEntity updateByRole(RoleView role) {
	// TODO 后期添加缺值处理
	RoleEntity entity = this.roleJpaRepository.findById(role.getId()).get();

	for (Object columnType : RequestHelper.getPatchColumns()) {
	    switch ((ColumnType) columnType) {
	    case NAME:
		entity.setName(role.getName());
		break;
	    case PERMISSIONS:
		Set<AclEntity> permissions = new HashSet<>();
		List<Integer> ids = Lists.newArrayList(role.getPermissions());
		Set<Integer> newPermissions = new HashSet<>();
		// 过滤出不存在的role
		// 将新的role插入

		entity.getAcls().stream().filter(permission -> ids.contains(permission.getId()))
			.forEach(permissions::add);
		List<Integer> temp = permissions.parallelStream().map(AclEntity::getId)
			.collect(Collectors.toList());
		ids.stream().filter(id -> !temp.contains(id)).forEach(newPermissions::add);

		if (newPermissions.size() < 1) {
		    if (entity.getAcls().size() == permissions.size()) {
			return entity;
		    }
		}
		permissions.addAll(this.permissionJpaRepository.findAllById(newPermissions));
		entity.setAcls(permissions);
		break;
	    case STATUS:
		entity.setStatus(role.getStatus());
		// TODO 添加其他字段校验
	    default:
		break;
	    }
	}
	
	return entity;
    }
    
    @Transactional
    public void clearRelation(RoleEntity role) {
	role.setAcls(new HashSet<>());
    }
    
    @Transactional
    public void delete(Integer id) {
	// TODO 级联删除逻辑
	this.roleJpaRepository.deleteById(id);
    }
}
