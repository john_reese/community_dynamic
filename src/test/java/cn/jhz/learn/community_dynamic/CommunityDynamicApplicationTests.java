package cn.jhz.learn.community_dynamic;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import cn.jhz.learn.community_dynamic.common.util.MineCommonUtil;
import cn.jhz.learn.community_dynamic.dao.LoginIdJpaRepository;
import cn.jhz.learn.community_dynamic.dao.PostClassJpaRepository;
import cn.jhz.learn.community_dynamic.dao.PostJpaRepository;
import cn.jhz.learn.community_dynamic.dao.TopicClassJpaRepository;
import cn.jhz.learn.community_dynamic.dao.TopicJpaRepository;
import cn.jhz.learn.community_dynamic.dao.UserJpaRepository;
import cn.jhz.learn.community_dynamic.manager.LoginIdManager;
import cn.jhz.learn.community_dynamic.model.LoginIdEntity;
import cn.jhz.learn.community_dynamic.model.PostEntity;
import cn.jhz.learn.community_dynamic.model.UserEntity;
import cn.jhz.learn.community_dynamic.service.StatsService;
import net.bytebuddy.asm.Advice.This;



@SpringBootTest
class CommunityDynamicApplicationTests {
    
    @Autowired
    private PostJpaRepository postJpaRepository;
    
    @Autowired
    private PostClassJpaRepository postClassJpaRepository;
    
    @Autowired
    private UserJpaRepository userJpaRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;
    
    @Autowired
    private TopicJpaRepository topicJpaRepository;
    
    @Autowired
    private TopicClassJpaRepository topicClassJpaRepository;
    
    @Autowired
    private StatsService statsService;
    
    @Test
    @Transactional
    public void test1() {
	
	for (Object view: this.statsService.countAllLinePostCount()) {
	    List<?> o = (List<?>) view;
	    System.out.println(o.get(0) +"_" + o.get(1));
	}
//	int length = MineCommonUtil.yearSubtract(MineCommonUtil.getCurrentYear(), this.postJpaRepository.findTopByOrderByCreateTimeAsc().get().getCreateTime());
//	    System.out.println(length);

//	System.out.println(this.postJpaRepository.findTopByOrderByCreateTimeAsc());
//	
//	System.out.println(this.postJpaRepository.countPostEntity_supportUsers_UserEntityByUser_idAndStatus(4, (byte) 1));
    }
    
//    @Test
//    public void test2() {
//	codeService.sendVerificationCodeByEmail("john_b_reese@163.com");
//    }
}
